import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms"; 
import { PaymentService } from "./../shared/services/payment.service";  
import { DevicesService } from "./../shared/services/devices.service";

import { SuccessPageComponent } from "./success-page.component";
import { SuccessComponent } from './components/success/success.component';
import { ErrorComponent } from './components/error/error.component';
import { ResultsComponent } from './container/results/results.component';
 

@NgModule({
  declarations: [
    SuccessPageComponent,
    SuccessComponent,
    ErrorComponent,
    ResultsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    SuccessPageComponent
  ],
  providers: [
    PaymentService,
    DevicesService
  ]
})
export class SuccessPageModule { }
