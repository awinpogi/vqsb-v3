import { Component, OnInit } from '@angular/core'; 
import { PaymentService } from "../../../shared/services/payment.service"; 
import { DevicesService } from "../../../shared/services/devices.service"; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  paymentCallback = JSON.parse(localStorage.getItem('paymentData'));  
  signupID = JSON.parse(localStorage.getItem('signupID')); 
  planTypeID = JSON.parse(localStorage.getItem('planTypeID'));
  paymentResult = {};  
  paymentData = {};

  constructor(
    private paymentConfig: PaymentService,
    private router: Router,
    private device: DevicesService 
  ) { }

  ngOnInit() { 

    localStorage.setItem('successpage', 'false') 

    if(this.paymentCallback && this.paymentCallback != '' && this.paymentCallback != null && this.paymentCallback != undefined) {
      this.paymentEvent(this.paymentCallback); 
    } else {
      if(localStorage.getItem('payment') != 'true' && this.router.url.includes('/refer/'))  {

        if(this.planTypeID == "1") {
          this.router.navigateByUrl('/refer/freedom/payment')
        } else if(this.planTypeID == "2") {
          this.router.navigateByUrl('/refer/max/payment')
        } else if(this.planTypeID == "3") {
          this.router.navigateByUrl('/refer/raptor/payment')
        }
        return;

      } else  if(localStorage.getItem('payment') != 'true' && this.router.url.includes('/internal/'))  {

        if(this.planTypeID == "1") {
          this.router.navigateByUrl('/internal/freedom/payment')
        } else if(this.planTypeID == "2") {
          this.router.navigateByUrl('/internal/max/payment')
        } else if(this.planTypeID == "3") {
          this.router.navigateByUrl('/internal/raptor/payment')
        }
        return;

      } else if (localStorage.getItem('payment') != 'true') {

        if(this.planTypeID == "1") {
          this.router.navigateByUrl('/freedom/payment')
        } else if(this.planTypeID == "2") {
          this.router.navigateByUrl('/max/payment')
        } else if(this.planTypeID == "3") {
          this.router.navigateByUrl('/raptor/payment')
        }
        return;

      }
    }
  }

  paymentEvent(response) {

    const theResponse = JSON.stringify(response)

    if( response.success == 'true') {

      this.paymentData = {
        "accept_t_n_c": 1,
        "cc_payment": response.refId,
        "creditCardAddress1": response.creditCardAddress1,
        "creditCardCountry": response.creditCardCountry,
        "creditCardCity": response.creditCardCity,
        "creditCardPostalCode": response.creditCardPostalCode,
        "email": response.email,
        "phone": response.phone,
        "paymentResponse": 'Success',
        "autoPay": "true",
        "update_signup_id": this.signupID,
        "ip": localStorage.getItem("ip"),
        "device": localStorage.getItem("device"),
        "signup_flow": 6  
      } 
     } else {

      this.paymentData = {
        "accept_t_n_c": 1,
        "cc_payment": "4028e488361ddbfc01362c21e8fe3aba",
        "paymentResponse": response.errorMessage,
        "autoPay": "false",
        "update_signup_id": this.signupID,
        "ip": localStorage.getItem("ip"),
        "device": localStorage.getItem("device"),
        "signup_flow": 6 
      }
    }
    
    /*
      Save and clear payment data
    */

    // console.log()
    if(response) { 
      this.paymentConfig.savePayment(this.paymentData).subscribe((res) => { 
        if(res) {

        this.device.getUserDevice().subscribe((res) => {
          localStorage.setItem("device", res['device'])
          localStorage.setItem("ip", res['ip'])
        })
        
        this.paymentResult = {
          "update_signup_id": this.signupID,
          "ip": localStorage.getItem("ip"),
          "device": localStorage.getItem("device"),
          "signup_flow": 7
        };  
          this.paymentConfig.saveSucess(this.paymentResult).subscribe((result) => {  
            localStorage.clear(); 
          })
        }
  
      })
    } else {
      if (this.router.url.includes('/refer/'))  {  
        if(this.planTypeID == 1) {
          this.router.navigateByUrl('/refer/freedom');
        } else if(this.planTypeID == 2) {
          this.router.navigateByUrl('/refer/max');
        } else if(this.planTypeID == 3) {
          this.router.navigateByUrl('/refer/raptor');
        }
      } else if (this.router.url.includes('/internal/'))  {  
        if(this.planTypeID == 1) {
          this.router.navigateByUrl('/internal/freedom');
        } else if(this.planTypeID == 2) {
          this.router.navigateByUrl('/internal/max');
        } else if(this.planTypeID == 3) {
          this.router.navigateByUrl('/internal/raptor');
        }
      } else {
        if(this.planTypeID == 1) {
          this.router.navigateByUrl('/freedom');
        } else if(this.planTypeID == 2) {
          this.router.navigateByUrl('/max');
        } else if(this.planTypeID == 3) {
          this.router.navigateByUrl('/raptor');
        }
      }
    }
  }

}
