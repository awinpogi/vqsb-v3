import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  
  utmSource;
  utmCampaign;
  utmMedium;
  utmTerms
  utmContent
  
  constructor( 
    private utmRoutes: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.utmSource = localStorage.getItem('utmSource' );
    this.utmCampaign = localStorage.getItem('utmCampaign' );
    this.utmMedium = localStorage.getItem('utmMedium' );
    this.utmTerms = localStorage.getItem('utmTerms' );
    this.utmContent = localStorage.getItem('utmContent' );
  }

}
