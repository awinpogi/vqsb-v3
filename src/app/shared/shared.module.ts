import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";

import { GoogleAnalyticsComponent } from "./google-analytics/google-analytics.component";
import { FormBasicComponent } from "../signup-form/components/form-basic/form-basic.component";
import { ShareComponent } from "./share.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { PipeComponent } from './pipe/pipe/pipe.component';
import { SafePipe } from './pipe/safe.pipe';

@NgModule({
  declarations: [
    GoogleAnalyticsComponent,
    FormBasicComponent,
    ShareComponent, 
    ContactUsComponent, PipeComponent, SafePipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    GoogleAnalyticsComponent,
    FormBasicComponent,
    ShareComponent,
    ContactUsComponent
  ]
})
export class SharedModule { }
