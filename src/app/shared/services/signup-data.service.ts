import { Observable } from 'rxjs';
import { AccountData } from "./../models/signup.model";
import { Existing } from "./../models/signup.model";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from "./../constants/url.constants";
import { EXTERNAL_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class SignupDataService {

  private apiUrl: string;
  private apiExisting: string;


  constructor(private httpClient: HttpClient) {
    this.apiUrl = API_BASE_URL + 'signup/update-details/signup-form?entity=vqmy';
    this.apiExisting = EXTERNAL_BASE_URL + 'salesforce/checkExisting.php/';
  }

    public saveData(accountData: AccountData) {
    return this.httpClient.post(`${this.apiUrl}`, accountData);
  }

    public checkExisting(nricNo: string) { 
    const formData = new FormData();   
    formData.append('nric', nricNo);
    return this.httpClient.post(`${this.apiExisting}`, formData);
    
  } 



}
