import { Observable } from 'rxjs';
import { Signup } from "./../models/signup.model";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { API_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  public plans: string;

  private apiUrl: string;


  constructor(private httpClient: HttpClient) {
      this.apiUrl = API_BASE_URL + 'new-signup?entity=vqmy';
  }
 

  public createSignup(signup: Signup) {
    return this.httpClient.post(`${this.apiUrl}`,signup);
  }

}
