import { TestBed } from '@angular/core/testing';

import { PromoReferralCodeService } from './promo-referral-code.service';

describe('PromoReferralCodeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PromoReferralCodeService = TestBed.get(PromoReferralCodeService);
    expect(service).toBeTruthy();
  });
});
