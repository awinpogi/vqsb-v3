import { TestBed } from '@angular/core/testing';

import { SignupDataService } from './signup-data.service';

describe('SignupDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignupDataService = TestBed.get(SignupDataService);
    expect(service).toBeTruthy();
  });
});
