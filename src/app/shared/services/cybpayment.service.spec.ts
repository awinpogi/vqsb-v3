import { TestBed } from '@angular/core/testing';

import { CybpaymentService } from './cybpayment.service';

describe('CybpaymentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CybpaymentService = TestBed.get(CybpaymentService);
    expect(service).toBeTruthy();
  });
});
