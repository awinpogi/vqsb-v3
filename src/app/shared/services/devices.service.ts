import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  private apiUrl: string  
  constructor(private httpClient: HttpClient) { 
    this.apiUrl = API_BASE_URL + 'eu-device';
  }

  public getUserDevice(): Observable<any> {
    return this.httpClient.get(this.apiUrl);
  } 
  
}