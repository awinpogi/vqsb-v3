import { PlansDetails } from "./../models/plandetails";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class PlanDetailsService {

  private apiUrl: string;


  constructor(private httpClient: HttpClient) {
    this.apiUrl = API_BASE_URL + 'signup/update-details/plan-detail';
  }

  public saveData(planDetailsData: PlansDetails) {
    return this.httpClient.post(`${this.apiUrl}`, planDetailsData);
  }

}
