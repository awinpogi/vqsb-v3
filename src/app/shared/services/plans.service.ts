import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class PlansService {

  public plans: string; 
  private apiUrl: string;


  constructor(
    private httpClient: HttpClient
    ) {
    this.apiUrl = API_BASE_URL + 'plans';
   }

  public getPlanAll(): Observable<any> {
    // Due to stackblitz can't get the local access I put this value to another api source
    
    return this.httpClient.get(this.apiUrl);
  }

  public getPlanByType(planType: number): Observable<any> {
      return this.httpClient.get(this.apiUrl + '/' + planType +'?entity=vqmy');
  }

  public getPlanByID(planType: number ,planId: number): Observable<any> { 
      return this.httpClient.get(this.apiUrl + '/' + planType + '/' + planId +'?entity=vqmy');
  } 


}
