import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NationalitiesService {

  public nationality: string;


  constructor(private httpClient: HttpClient) { }

  public getNationalityJSON(): Observable<any> {
    // Due to stackblitz can't get the local access I put this value to another api source
    const apiUrl = './assets/json/nationalities.json';
    return this.httpClient.get(apiUrl);
  }

}
