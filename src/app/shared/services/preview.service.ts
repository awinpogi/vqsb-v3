import { Observable } from 'rxjs'; 
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from "./../constants/url.constants";
import { SAF } from "./../models/signup.model";

@Injectable({
  providedIn: 'root'
})
export class PreviewService {

  public plans: string;

  private apiUrl: string;
  private safURL: string;
  private saveSAF: string;


  constructor(private httpClient: HttpClient) {
    this.apiUrl = API_BASE_URL + 'signup/get-details/preview';
    this.safURL = API_BASE_URL + 'saf/get-details';
      this.saveSAF = API_BASE_URL + 'saf/update-details?entity=vqmy';
   }

  public showPreview(signupId: number): Observable<any> {
    return this.httpClient.get(this.apiUrl +'/'+ signupId +'?entity=vqmy');
  } 

  public getSAFData(signupId: number): Observable<any> {
      return this.httpClient.get(this.safURL + '/' + signupId + '?entity=vqmy');
  }
  
  public saveSAFURL(safData: SAF): Observable<any> {
    return this.httpClient.post(`${this.saveSAF}`,safData);
  } 
  

}
