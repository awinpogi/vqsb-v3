import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EXTERNAL_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class UploadService { 

  private apiUrl: string; 

  constructor(private httpClient: HttpClient) {
    this.apiUrl = EXTERNAL_BASE_URL + 'uploads';
   }

  /*
   Upload NRIC Front Copy
  */
  public uploadNricFront(data) {
    let uploadURL = `${this.apiUrl}/nricfront.php`;
    return this.httpClient.post<any>(uploadURL, data);
  }
  
  /*
   Upload NRIC Front Copy
  */
  public uploadNricBack(data) {
    let uploadURL = `${this.apiUrl}/nricback.php`;
    return this.httpClient.post<any>(uploadURL, data);
  }

  /*
   Generate SAF
  */
 public generateSAF(data) {
  let uploadURL = `${this.apiUrl}/saf_creator.php`;
  return this.httpClient.post<any>(uploadURL, data);
}

}
