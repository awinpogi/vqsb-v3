import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_BASE_URL } from "./../constants/url.constants";
import { EXTERNAL_BASE_URL } from "./../constants/url.constants";
import { Payment } from "./../../cyb-payment/payment";
import { headersToString } from 'selenium-webdriver/http';

 
@Injectable({
    providedIn: 'root'
})



export class CybpaymentService {

    private genSignedDataURL: string;
    private cybURL: string;
    private cybConnect: string;
    private successURL: string;
    private getCYBData: string;
   

    constructor(private httpClient: HttpClient) {
        this.genSignedDataURL = EXTERNAL_BASE_URL + 'zuora/generateSignedData.php';
        this.cybURL = 'https://testsecureacceptance.cybersource.com/silent/pay';
        this.cybConnect = EXTERNAL_BASE_URL + 'zuora/cybConnect.php';
        this.getCYBData = EXTERNAL_BASE_URL + 'zuora/cybsecurity.php';
        

    }

    public performGetEX(): Observable<any> {
        
        return this.httpClient.get(this.genSignedDataURL);
    }

    public postCybData(cybPostData: Payment): Observable<any> {
        const httpOptions = new HttpHeaders({
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',

        });
        //return this.httpClient.post(this.cybConnect, cybPostData, { headers: httpOptions });
        return this.httpClient.post(this.cybConnect, cybPostData);
    }

    public postSignedData(cybPaymentData: Payment): Observable<any> {
        const httpOptions = new HttpHeaders({
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',

        });
        return this.httpClient.post(this.genSignedDataURL, cybPaymentData, { headers: httpOptions });
    }


}
