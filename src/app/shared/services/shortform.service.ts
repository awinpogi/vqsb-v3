import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_BASE_URL } from "./../constants/url.constants"; 


@Injectable({
  providedIn: 'root'
})
export class ShortformService {

  private apiUrl: string  
  constructor(private httpClient: HttpClient) {  
    this.apiUrl = API_BASE_URL + 'signup/get-details/signup-form/';
  }

  public getShortData(): Observable<any> { 
      return this.httpClient.get(this.apiUrl +'?entity=vqmy');
  }

  public getShortDataByID(singupID: string): Observable<any> {
      return this.httpClient.get(this.apiUrl + singupID +'?entity=vqmy');
  } 
  
}
