import { TestBed } from '@angular/core/testing';

import { PlanDetailsService } from './plan-details.service';

describe('PlanDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanDetailsService = TestBed.get(PlanDetailsService);
    expect(service).toBeTruthy();
  });
});
