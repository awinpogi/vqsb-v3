import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PromoCode } from "./../models/shortform.model";
import { API_BASE_URL } from "./../constants/url.constants";
import { EXTERNAL_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class PromoReferralCodeService {

  private referralURL: string; 
  private promoCodeURL: string; 

  constructor(
    private httpClient: HttpClient
  ) { 
    this.referralURL = EXTERNAL_BASE_URL + 'salesforce/checkReferral.php';
    this.promoCodeURL = API_BASE_URL + 'check-voucher';
  }

  public checkReferralCode(referralCode: string) { 
    const formData = new FormData();   
    formData.append('referralCode', referralCode);
    return this.httpClient.post(`${this.referralURL}`, formData);
    
  } 

  public checkPromoCode(promoCode: PromoCode) {  
    return this.httpClient.post(`${this.promoCodeURL}`, promoCode);
  } 
}
