import { TestBed } from '@angular/core/testing';

import { LongformService } from './longform.service';

describe('LongformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LongformService = TestBed.get(LongformService);
    expect(service).toBeTruthy();
  });
});
