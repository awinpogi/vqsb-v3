import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Payment } from "./../models/payment.model"; 
import { Success } from "./../models/payment.model";
import { API_BASE_URL } from "./../constants/url.constants";
import { EXTERNAL_BASE_URL } from "./../constants/url.constants";

@Injectable({
  providedIn: 'root'
})
export class PaymentService { 

  private apiUrl: string; 
  private paymentUrl: string; 
  private successUrl: string; 

  constructor(private httpClient: HttpClient) {
      this.apiUrl = EXTERNAL_BASE_URL + 'zuora/generateSignature.php?entity=vqmy';
    this.paymentUrl = API_BASE_URL + 'signup/update-details/payment?entity=vqmy';
      this.successUrl = API_BASE_URL + 'signup/update-details/success?entity=vqmy';
   }

  public getSignature(): Observable<any> {
    return this.httpClient.get(this.apiUrl);
  }
  
  public generatePaymentSignature(id) {
    return this.httpClient.post(`${this.apiUrl}`, id);
  }

  public savePayment(paymentData: Payment) {
    return this.httpClient.post(`${this.paymentUrl}`, paymentData);
  }

  public saveSucess(Success: Success) {
    return this.httpClient.post(`${this.successUrl}`, Success);
  }


}
