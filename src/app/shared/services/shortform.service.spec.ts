import { TestBed } from '@angular/core/testing';

import { ShortformService } from './shortform.service';

describe('ShortformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShortformService = TestBed.get(ShortformService);
    expect(service).toBeTruthy();
  });
});
