import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ShortformService } from './../services/shortform.service';
import { Store } from './store';
import { User } from '../models/shortform.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShortFormStore extends Store<User[]> {

  constructor(private postsService: ShortformService) {
    super();
  }z

  init = (): void => {
    if (this.getAll()) { return; }

    // this.postsService.getUser().pipe(
    //   tap(this.store)
    // ).subscribe();
  }
}