import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { PlansService } from './../services/plans.service';
import { Store } from './store';
import { Plans } from "./../models/plans.model";
 
@Injectable({
  providedIn: 'root'
})
export class PlansStore extends Store<Plans[]> {
 
  constructor(private planService: PlansService) {
    super();
  }

 
}
