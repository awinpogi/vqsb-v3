
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { SignupService } from './../services/signup.service';
import { Store } from './store';
import { Signup } from "./../models/signup.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignupStore extends Store<Signup[]> {

  constructor(private signupService: SignupService) {
    super();
  }


}
