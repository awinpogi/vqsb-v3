interface Scripts {
    name: string;
    src: string;
}  
export const ScriptStore: Scripts[] = [
    {name: 'zuoraPayment', src: 'https://static.zuora.com/Resources/libs/hosted/1.2.0/zuora-min.js'}
];