

import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { PostsService } from '../services/posts.service';
import { Store } from './store';
import { Post } from '../models/post.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsStore extends Store<Post[]> {

  constructor(private postsService: PostsService) {
    super();
  }

  init = (): void => {
    if (this.getAll()) { return; }

    this.postsService.get$().pipe(
      tap(this.store)
    ).subscribe();
  }

  getId$ = (postId: number) => this.postsService
    .getId$(postId)
    .pipe(
      tap(resPost => {
         const posts = this.getAll();
        const postIndex = this.getAll().findIndex(item => item.signup_id === postId);
        posts[postIndex] = {
            signup_meta_id: resPost.signup_meta_id,
            signup_id: resPost.signup_id,
            signup_meta_key: resPost.signup_meta_key,
            signup_meta_value: resPost.signup_meta_value
        };

        this.store(posts);
      })
    )

  create$ = (post: Post): Observable<Post> => this.postsService
    .post$(post)
    .pipe(
      tap(resPost => {
        this.store([
          ...this.getAll(),
          {
            signup_id: resPost.signup_id,
            ...post
          }
        ]);
      })
    )

  update$ = (postId: number, post: Post) => this.postsService
    .patch$(postId, post)
    .pipe(
      tap(resPost => {
        const posts = this.getAll();
        const postIndex = this.getAll().findIndex(item => item.signup_id === postId);
        posts[postIndex] = {
            signup_id: resPost.signup_id,
          ...post
        };

        this.store(posts);
      })
    )

  delete$ = (postId: number) => this.postsService
    .delete$(postId)
    .pipe(
      tap(() => {
        const posts = this.getAll();
        const postIndex = this.getAll().findIndex(item => item.signup_id === postId);
        posts.splice(postIndex, 1);

        this.store(posts);
      })
    )
}
