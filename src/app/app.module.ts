import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlansStore } from './shared/stores/plan.store';
import { PlansService } from './shared/services/plans.service'; 
import { CybpaymentService } from './shared/services/cybpayment.service'; 


import { AppComponent } from './app.component';
import { HomeModule } from "./home/home.module";
import { SignupModule } from "./signup-form/signup.module";
import { PlanDetailsModule } from './plan-details/plan-details.module';
import { RegistrationFormModule } from "./registration-form/registration-form.module";
import { PreviewModule } from "./preview/preview.module";
import { PaymentModule } from "./payment/payment.module";
import { SuccessPageModule } from "./success-page/success-page.module";
import { LocationModule } from "./location/location.module";
import { SharedModule } from "./shared/shared.module";
import { HttpClientModule } from "@angular/common/http"; 
import { MainRoutes } from "./app-routing.module";
import { ReferRoutes } from "./app-routing.module";
import { InternalRoutes } from "./app-routing.module";

import { ReactiveFormsModule } from '@angular/forms';
import { CustomValidators } from './shared/services/custom_validators'; 
import { FormsModule } from '@angular/forms';
import { NameEditorComponent } from './name-editor/name-editor.component';
import { CybPaymentComponent } from './cyb-payment/cyb-payment.component';
import { CybtestComponent } from './cybtest/cybtest.component';
import { CybsuccessComponent } from './cybsuccess/cybsuccess.component';
// import { FormService } from './shared/services/form';



@NgModule({
  declarations: [
    AppComponent,
    NameEditorComponent,
    CybPaymentComponent,
    CybtestComponent,
    CybsuccessComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
	LocationModule,
    SignupModule,
    PlanDetailsModule,
    RegistrationFormModule,
    PreviewModule,
    PaymentModule,
    SuccessPageModule,
    SharedModule,
    HttpClientModule, 
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(MainRoutes),
    RouterModule.forRoot(InternalRoutes),
    RouterModule.forRoot(ReferRoutes),
  ],
  providers: [
    CustomValidators,
    PlansStore,
    PlansService,
    CybpaymentService
    // FormService
  ],
  exports: [
    RouterModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
