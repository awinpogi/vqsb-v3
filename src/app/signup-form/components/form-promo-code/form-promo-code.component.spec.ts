import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPromoCodeComponent } from './form-promo-code.component';

describe('FormPromoCodeComponent', () => {
  let component: FormPromoCodeComponent;
  let fixture: ComponentFixture<FormPromoCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPromoCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPromoCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
