import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-form-promo-code',
  templateUrl: './form-promo-code.component.html',
  styleUrls: ['./form-promo-code.component.css']
})
export class FormPromoCodeComponent implements OnInit {

  type = "hidden";
  class = "d-none";
  isReadOnly = false;
  @Output() formReady = new EventEmitter<FormGroup>();
  submitted = false;

  promoForm: FormGroup;
  @Input() shortFormSubmitted;
  @Input() checkPromoCode;
  @Input() showPromo;

  constructor(
    private formBuilder: FormBuilder,
    ) { }

  ngOnInit() { 
  }
 
  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    if(changes['showPromo']) {
      if(changes['showPromo'].currentValue == true) {
        this.promoForm = this.formBuilder.group({
          promoCode: ['', [Validators.required]],
        });
        this.formReady.emit(this.promoForm);
      } else {
        this.promoForm = this.formBuilder.group({
          promoCode: [''],
        });
        this.formReady.emit(this.promoForm);
      }
    }
  }

  get f() { return this.promoForm.controls; }

}
