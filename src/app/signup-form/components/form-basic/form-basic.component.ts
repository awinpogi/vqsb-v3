import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy, AfterViewChecked, SimpleChanges } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { NationalitiesService } from "../../../shared/services/nationalities.service";
import { UploadService } from "../../../shared/services/upload.service";
import Swal from 'sweetalert2' 
import * as moment from "moment"; 
import { Router } from '@angular/router';

export function DateValidator(format = "YYYY-MM-DD"): any {
  return (control: FormControl): { [key: string]: any } => {
    const val = moment(control.value, format, true);

    if (!val.isValid()) {
      return { invalidDate: true };
    } else {
      return { invalidDate: false };
    }
  };
}

@Component({
  selector: 'app-form-basic',
  templateUrl: './form-basic.component.html',
  styleUrls: ['./form-basic.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormBasicComponent implements OnInit {
  

  @Output() formReady = new EventEmitter<FormGroup>();

  nationalityData: any[];
  basicForm: FormGroup;
  submitted = false;
  @Input() isLongForm = true;
  @Input() shortFormSubmitted;
  @Input() longFormSubmitted;
  @Input() info;
  firstName: string;
  lastName: string;
  emailAddress: string;
  phoneNumber: number;
  nricUploadResponse;
  isFrontSuccess = false;
  isBackSuccess = false;
  labelFront = "Choose file";
  labelBack = "Choose file";
  IsRefer = false;
  floodLightURL;
  floodLightID;
  

  namePattern = '^[a-zA-Z0-9 ]+$';
  phonePattern = '^[0-9]+$';

  validation_message = {
    'firstName': [
      { type: 'required', message: 'First name is required' },
      { type: 'minlength', message: 'First name must be at least 5 characters long' },
      { type: 'maxlength', message: 'First name cannot be more than 40 characters long' },
      { type: 'pattern', message: 'First name must contain only numbers and letters' }
    ],
    'lastName': [
      { type: 'required', message: 'Last name is required' },
      { type: 'minlength', message: 'Last name must be at least 5 characters long' },
      { type: 'maxlength', message: 'Last name cannot be more than 40 characters long' },
      { type: 'pattern', message: 'Last name must contain only numbers and letters' }
    ],
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Email must be valid' }
    ],
    'mobile': [ 
      { type: 'required', message: 'Mobile number is required' },
      { type: 'minlength', message: 'Mobile number must be at least 8 characters long' },
      { type: 'maxlength', message: 'Mobile number cannot be more than 8 characters long' },
      { type: 'pattern', message: 'Mobile must contain only numbers' }
    ],
    'dob': [ 
      { type: 'required', message: 'Birthdate is required' },
      { type: 'date', message: 'Birthday must be valid date' }
    ],
    'gender': [ 
      { type: 'required', message: 'Gender is required' },
    ],
    'nric': [
      { type: 'required', message: 'NRIC # is required' },
      { type: 'minlength', message: 'NRIC # must be valid' }, 
      { type: 'pattern', message: 'NRIC # must contain only numbers and letters' }
    ], 
    'nationality': [
      { type: 'required', message: 'Nationality is required' },
    ], 
    'postalcode': [
      { type: 'required', message: 'Postal code is required' },
      { type: 'minlength', message: 'Postal code must be at least 6 number long' },
      { type: 'maxlength', message: 'Postal code cannot be more than 6 number long' },
      { type: 'pattern', message: 'Postal code must contain only numbers' }
    ], 
    'floorno': [
      { type: 'maxlength', message: 'Floor number cannot be more than 3 characters long' },
      { type: 'pattern', message: 'Floor number must contain only numbers and letters' }
    ], 
    'unitno': [
      { type: 'maxlength', message: 'Unit number cannot be more than 2 characters long' },
      { type: 'pattern', message: 'FlooUnitr number must contain only numbers' }
    ], 
    'nricfront': [
      { type: 'required', message: 'NRIC Front copy is required' },
    ],
    'nricback': [
      { type: 'required', message: 'NRIC Back copy is required' },
    ], 
    'promoter': [
      { type: 'required', message: 'Promoter Name is required' },
    ],
    'others': [
      { type: 'required', message: 'Promoter Name is required' },
      { type: 'pattern', message: 'Promoter Name must contain only numbers and letters' }
     ]
  }
  constructor(
    private formBuilder: FormBuilder,
    private appConfig: NationalitiesService,
    private nricUpload: UploadService,
    private router: Router, 
    ) { 
  }

  ngOnInit() { 

    if (this.router.url.includes('/refer/'))  {  
      this.IsRefer = true;
    } else {
      this.IsRefer = false;
    } 
 
    this.floodLightID = this.generateUID();
    this.floodLightURL = 'https://5691183.fls.doubleclick.net/activityi;src=5691183;type=resid002;cat=signu008;u2='+this.floodLightID+';dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?';
    
    this.dataForm();
    this.nationality(); 

  }

  get f() { return this.basicForm.controls; }

  ngOnChanges(changes: SimpleChanges) {  
    if(this.isLongForm == true && changes['info']){
      if (changes['info'].currentValue != "") {
        // console.log(changes['info'].currentValue)
        this.firstName = changes['info'].currentValue['FirstName'];
        this.lastName = changes['info'].currentValue['LastName'];
        this.emailAddress = changes['info'].currentValue['PersonEmail'];
        this.phoneNumber = changes['info'].currentValue['Phone'];

        this.dataForm();
      }
    }
  }
  
  /*
    Initialized Form input 
  */

  dataForm() {
      if (this.isLongForm === true) {
      this.basicForm = this.formBuilder.group({
        firstName: [this.firstName, Validators.required],
        lastName: [this.lastName, Validators.required],
        email: [this.emailAddress, [Validators.required, Validators.email]],
        mobile: [this.phoneNumber,
          [ 
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(8),
            Validators.pattern(this.phonePattern),
          ]
        ],
        dob: ['',
          [ 
            Validators.required
          ]
        ],
        gender: ['',
          [ 
            Validators.required
          ]
        ],
        postalcode: ['',
          [ 
            //Validators.required,
            Validators.minLength(6),
            Validators.maxLength(6),
            Validators.pattern(this.phonePattern),
          ]
        ],
        floorno: ['',
          [ 
            Validators.maxLength(3),
            Validators.pattern(this.phonePattern),
          ]
        ],
        unitno: ['',
          [ 
            Validators.pattern(this.phonePattern),
          ]
        ],
        nationality: ['',
          [ 
            Validators.required
          ]
        ],
        nricno: ['',
          [ 
            Validators.minLength(5),
            Validators.pattern(this.namePattern),
            Validators.required
          ]
        ],
        nricfront: ['',
          [ 
            Validators.required
          ]
        ],
        nricback: ['',
          [ 
            Validators.required
          ]
        ],
      });
      } else {
 
          this.basicForm = this.formBuilder.group({
             
        firstName: ['',
          [ 
            Validators.maxLength(40),
            Validators.pattern(this.namePattern),
            Validators.required
          ]
        ],
        lastName: ['',
          [ 
            Validators.maxLength(40),
            Validators.pattern(this.namePattern),
            Validators.required
          ]
        ],
        email: ['',
          [ 
            Validators.email,
            Validators.required
          ]
        ],
        mobile: ['',
          [ 
            Validators.minLength(8),
            Validators.maxLength(8),
            Validators.pattern(this.phonePattern),
          ]
        ],
        promoterName: ['',
          [ 
            Validators.required
          ]
        ],
        otherPromoter: ['',
          [ 
            Validators.required
          ]
        ],
        customerUID: this.floodLightID
      }); 
    }

   
    this.formReady.emit(this.basicForm); 
  }

  /*
    Disable future date
  */
  maxDate(): string {
    return new Date().toISOString().split('T')[0]
  }

  /*
    Disable under 18
  */
  minDate() {
  return new Date().getFullYear()-18 +'-'+ "12-31"
  }

  /*
    Generate 16 random characters for floodlight tracking
  */
 private generateUID(): string {
  return Math.random().toString(36).substring(2, 10) + Math.random().toString(36).substring(2, 10);
}

  /* 
    Initialized the value of nationalities dropdown
  */
  nationality() {
    this.appConfig.getNationalityJSON().subscribe(res => {
      this.nationalityData = res;
      this.appConfig.nationality = this.appConfig.nationality;
      // console.log(this.nationalityData);
    });

  }

  extractNameFromJson(obj){
    obj = JSON.parse(obj);
    return obj.name;
  }

  /* 
    Upload NRIC Front
  */ 
 onNRICFrontUpload(event) {
  if (event.target.files.length > 0) {

    const file = event.target.files[0];
    const formData = new FormData(); 
 
    this.labelFront = file.name;
    this.basicForm.controls['nricfront'].setValue(file); 

    formData.append('nricfront', this.basicForm.get('nricfront').value);

    this.nricUpload.uploadNricFront(formData).subscribe(
      (res) => {
        this.nricUploadResponse = res;
          if(res['error'] == false) {
            this.isFrontSuccess = true; 
            this.basicForm.controls['nricfront'].setValue(res['url']); 
          }
          this.uploadDialog(res['error'], 'NRIC Front', res['message']);
      },
      (err) => {  
        console.log(err);
      }
    );
  }
 }

   /* 
    Upload NRIC Back
  */ 
 onNRICBackUpload(event) {
  if (event.target.files.length > 0) {

    const file = event.target.files[0];
    const formData = new FormData(); 

    this.labelBack = file.name;

    this.basicForm.controls['nricback'].setValue(file); 

    formData.append('nricback', this.basicForm.get('nricback').value);

    this.nricUpload.uploadNricBack(formData).subscribe(
      (res) => {
        this.nricUploadResponse = res;
        if(res['error'] == false) {
          this.isBackSuccess = true;
          this.basicForm.controls['nricback'].setValue(res['url']); 
        }
        this.uploadDialog(res['error'], 'NRIC Back', res['message']);
      },
      (err) => {  
        console.log(err);
      }
    );
  }
 }
 
 /*
  Dialog box for NRIC upload
 */

 uploadDialog(errorStatus: boolean, errorTitle: string, errorText: string) { 
  if(errorStatus == false) {
    Swal.fire({
      type: 'success',
      title: errorTitle,
      text: errorText
    }) 
  } else {
    Swal.fire({
      type: 'error',
      title: errorTitle,
      text: errorText

    })
  }
 }
}
