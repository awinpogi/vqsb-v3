import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { FormPromoCodeComponent } from "../../components/form-promo-code/form-promo-code.component";
import { FormGroup, FormBuilder } from '@angular/forms';
import { ShortformService } from "./../../../shared/services/shortform.service";   
import { SignupDataService } from "./../../../shared/services/signup-data.service"; 
import { PromoReferralCodeService } from "./../../../shared/services/promo-referral-code.service";
import { Router } from '@angular/router';
import Swal from 'sweetalert2' 

@Component({
  selector: 'app-short-form',
  templateUrl: './short-form.component.html',
  styleUrls: ['./short-form.component.scss'],
})
export class ShortFormComponent implements OnInit {
  
  promoLink = 'd-block';
  promoButton = 'd-none';
  sourceLead = 'Web';
  signupID = JSON.parse(localStorage.getItem('signupID'));

  displayPromo = false;
  test: boolean = false;
  shortFormSubmitted = false;
  checkPromoCode = false;
  value = false;
  IsRefer = false;
  isValidPromo = false;

  shortForm: FormGroup 
  customerInfo: any[] 
  accountData:  {}

  @Output() public nextPage = new EventEmitter();
  @ViewChild(FormPromoCodeComponent) promoCode:FormPromoCodeComponent; 
 
  promoCodeInput
  signupLocation;
  referralData;
  plan: string; 
  basicDetails

  referrerName;
  referrersfID;
  referrerzAccountNumber
  referrerzID
  referrerCount;
  referrerCampagin;
  referrerCode;
  referrerContactID;

  promoter = [{
    "code": "",
  }]

  constructor(
    private formBuilder: FormBuilder,
    private shortFormData: ShortformService,
    private signupData: SignupDataService,
    private referralResult: PromoReferralCodeService,
    private router: Router
    ){
      this.customerInfo = [];
      this.initialize();  
     }
  
  ngOnInit() {  

    this.signupLocation = "Online";
    this.shortForm = this.formBuilder.group({
      id: this.signupID
    })  
  } 
  get f() { return this.shortForm.controls; }

  /* 
    Initialized the customer data
  */
  initialize() { 
    this.shortFormData.getShortDataByID(this.signupID).subscribe(res => {
        this.customerInfo = res; 
    });

  } 

  /**
   * After a form is initialized, we link it to our main form
   */
  formInitialized(name: string, form: FormGroup) {
    this.shortForm.setControl(name, form);
  }

  /*
    Display error dialog box 
  */
  errorDialog (error: string, errorTitle: string, errorText:string) {
    if(error == "success") {

      Swal.fire({
        type: 'success',
        title: errorTitle,
        text: errorText
      }) 

    } else {
      Swal.fire({
        type: 'error',
        title: errorTitle,
        text: errorText
      }) 
    }
  }

    /*
  Display error dialog box 
*/
    warningDialog(errorTitle: string, errorText: string) {
        Swal.fire({
            type: 'warning',
            title: errorTitle,
            text: errorText
        })
    }
  // Proceed to next page
    nextButton() { 
        this.shortFormSubmitted = true;
        let saluation;

        if (this.shortForm.value['basic']['gender'] == 'Male') {
            saluation = 'Mr.';
        } else {
            saluation = 'Ms.'
        }
        localStorage.setItem('reEmail', this.shortForm.value['basic']['email']);
        localStorage.setItem('reFirstName', this.shortForm.value['basic']['firstName']);
        localStorage.setItem('reLastName', this.shortForm.value['basic']['lastName']);
        localStorage.setItem('rePhone', this.shortForm.value['basic']['mobile']);

        console.log(this.shortForm)
        //console.log(this.shortForm.value['basic']['customerUID']);
        this.accountData = {
           
            "Salutation": saluation,
            "FirstName": this.shortForm.value['basic']['firstName'],
            "LastName": this.shortForm.value['basic']['lastName'],
            "Phone": this.shortForm.value['basic']['mobile'],
            "PersonEmail": this.shortForm.value['basic']['email'],
            "Nationality__c": this.shortForm.value['basic']['nationality'],
            "PersonBirthdate": this.shortForm.value['basic']['dob'],
            "Gender__c": this.shortForm.value['basic']['gender'],
            "Entity__c": "VQSB",
            "Sign_Up_Location__c": 'Online',
            "Malaysia_Sales_Agent__c": localStorage.getItem("myAgent"),
            "Building__c": localStorage.getItem("myBuilding"),
            "State__c": localStorage.getItem("myState"),
            "PersonOtherCountry": "Malaysia",
            "Status__c": "Active",
            "RecordTypeId": "0126F000000vgRE",
            "NRIC_Passport_FIN_No__pc": this.shortForm.value['basic']['nricno'],
            "UTM_Source__c": localStorage.getItem('utmSource'),
            "UTM_Medium__c": localStorage.getItem('utmMedium'),
            "UTM_Campaign__c": localStorage.getItem('utmCampaign'),
            "UTM_Term__c": localStorage.getItem('utmTerms'),
            "UTM_Content__c": localStorage.getItem('utmContent'),
            "Source_of_Lead__c": 'Worth of Mouth',
            "Signup_ID__c": this.shortForm.value['basic']['customerUID'],
            "update_signup_id": this.signupID,
            "referralCode": this.referrerCode,
            "ip": localStorage.getItem("ip"),
            "device": localStorage.getItem("device"),
            "signup_flow": 3,
            "nricfront": this.shortForm.value['basic']['nricfront'],
            "nricback": this.shortForm.value['basic']['nricback'],
            "PersonOtherStreet": localStorage.getItem("myStreet"),
            "PersonOtherPostalCode": localStorage.getItem("myPostCode"),
            "PersonMailingPostalCode": localStorage.getItem("myPostCode"),
            "PersonMailingStreet": localStorage.getItem("myStreet"),
        } 
        // stop here if form is invalid
        if (this.shortForm.invalid) {
            return;
        } else if (this.shortForm.valid) {
            this.signupData.checkExisting(this.shortForm.value['basic']['nricno']).subscribe(res => {
                if (res['success'] && res['success'] == 1) {
                    this.warningDialog('Existing Customer', 'Our system shows that you are an existing customer. Please contact our customer support')
                } else {
                    this.signupData.saveData(this.accountData).subscribe((res) => {
                        console.log(res)
                        localStorage.setItem('registration', 'true');
                        this.nextPage.emit(this.accountData);
                    })
                }
            });
        }
    }

  
}
