import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"; 
import { DevicesService } from "./../shared/services/devices.service"

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent implements OnInit {
  planTypeID = JSON.parse(localStorage.getItem('planTypeID'));
  constructor (
    private router: Router, 
    private device: DevicesService
  ) { }

  ngOnInit() { 

    this.device.getUserDevice().subscribe((res) => {
      localStorage.setItem("device", res['device'])
      localStorage.setItem("ip", res['ip'])
    })
    
    localStorage.setItem('shortform', 'false');

    /*  if (this.planTypeID == "1") {
          this.router.navigateByUrl('/freedom')
      } */
    
  }

    public nextEvent(data: any): void {
        this.router.navigateByUrl('/freedom/preview');
  }
       

}
