import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms"; 
import { SharedModule } from "../shared/shared.module";
import { ShortformService } from "../shared/services/shortform.service";
import { SignupStore } from "../shared/stores/signup.store";
import { PostsStore } from "../shared/stores/posts.store";
import { PostsService } from "../shared/services/posts.service";
import { SignupService } from './../shared/services/signup.service'; 
import { SignupDataService } from './../shared/services/signup-data.service';
import { PromoReferralCodeService } from './../shared/services/promo-referral-code.service'; 
import { SignupFormComponent } from "./signup-form.component"; 
import { ShortFormComponent } from './container/short-form/short-form.component';
import { FormPromoCodeComponent } from './components/form-promo-code/form-promo-code.component'; 
//import { FormLocationComponent } from './components/form-location/form-location.component'; 
import { DevicesService } from "./../shared/services/devices.service";
 

@NgModule({
  declarations: [
    SignupFormComponent,
    // FormBasicComponent,
    ShortFormComponent,
    FormPromoCodeComponent,
	//FormLocationComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    SignupFormComponent
  ], 
  providers: [
    ShortformService,
    SignupStore,
    PostsStore,
    PostsService,
    SignupService,
    SignupDataService,
    PromoReferralCodeService,
    DevicesService
  ]
})
export class SignupModule { }
