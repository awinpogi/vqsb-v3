import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms"; 
import { SharedModule } from "../shared/shared.module"; 
import { ShortformService } from "../shared/services/shortform.service";
import { LongformService } from "../shared/services/longform.service";
import { SignupDataService } from './../shared/services/signup-data.service';
import { UploadService } from "./../shared/services/upload.service";
import { DevicesService } from "./../shared/services/devices.service";


import { RegistrationFormComponent } from "./registration-form.component";
import { LongFormComponent } from './container/long-form/long-form.component';
import { FormNricComponent } from './components/form-nric/form-nric.component';

@NgModule({
  declarations: [
    RegistrationFormComponent,
    LongFormComponent,
    FormNricComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, 
    SharedModule
  ],
  exports: [
    RegistrationFormComponent
  ], 
  providers: [
    ShortformService,
    LongformService,
    SignupDataService,
    UploadService,
    DevicesService
  ]
})
export class RegistrationFormModule { }
