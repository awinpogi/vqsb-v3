import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LongformService } from "./../../../shared/services/longform.service"; 
import { SignupDataService } from "./../../../shared/services/signup-data.service";
import { UploadService } from "./../../../shared/services/upload.service";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-long-form',
  templateUrl: './long-form.component.html',
  styleUrls: ['./long-form.component.scss']
})
export class LongFormComponent implements OnInit {

  longForm: FormGroup;
  existingCustomer: any[]   
  customerInfo: any[]   
  accountData:  {}
  signupID = JSON.parse(localStorage.getItem('signupID'));
  longFormSubmitted = false;
  @Output() public nextPage = new EventEmitter();
  
  constructor(
    private formBuilder: FormBuilder, 
    private longFormData: LongformService,
    private signupData: SignupDataService, 
    private uploadData: UploadService 
  ) { 
    this.customerInfo = [];
    this.initialize();
  }

  ngOnInit() {
    this.longForm = this.formBuilder.group({
      id: this.signupID
    })
  }
  /* 
    Initialized the customer data
  */
  initialize() { 
    this.longFormData.getShortDataByID(this.signupID).subscribe(res => {
      this.customerInfo = res;   
      // console.log(res)
    }); 
  } 

  /*
    Display error dialog box 
  */
  warningDialog (errorTitle: string, errorText:string) {
    Swal.fire({
      type: 'warning',
      title: errorTitle,
      text: errorText
    }) 
  }

  nextButton() {
    this.longFormSubmitted = true;

    let saluation;

    if (this.longForm.value['basic']['gender'] == 'Male') {
      saluation = 'Mr.';
    } else {
      saluation = 'Ms.'
    }

    this.accountData = {
      "Salutation": saluation,
      "NRIC_Passport_FIN_No__pc": this.longForm.value['basic']['nricno'],
      "FirstName": this.longForm.value['basic']['firstName'],
      "LastName": this.longForm.value['basic']['lastName'],
      "Phone": this.longForm.value['basic']['mobile'], 
      "PersonMobilePhone": this.longForm.value['basic']['mobile'], 
      "PersonEmail": this.longForm.value['basic']['email'],
      "Gender__c": this.longForm.value['basic']['gender'],
      "PersonBirthdate": this.longForm.value['basic']['dob'],
      "Nationality__c": this.longForm.value['basic']['nationality'],
      "PersonOtherStreet": '#' + this.longForm.value['basic']['floorno'] + '-' + this.longForm.value['basic']['unitno'], 
      "PersonOtherPostalCode": this.longForm.value['basic']['postalcode'],
      "PersonOtherCountry": 'Singapore',
      "PersonMailingPostalCode": this.longForm.value['basic']['postalcode'],
      "PersonMailingStreet": '#' + this.longForm.value['basic']['floorno'] + '-' + this.longForm.value['basic']['unitno'], 
      "PersonMailingCountry": 'Singapore',
      "nricfront": this.longForm.value['basic']['nricfront'],
      "nricback": this.longForm.value['basic']['nricback'],
      "update_signup_id": this.signupID,
      "ip": localStorage.getItem("ip"),
      "device": localStorage.getItem("device"),
      "signup_flow": 4
    } 

    // stop here if form is invalid
    if (this.longForm.invalid) {
      return;
    } else if(this.longForm.valid) {
      // console.log(this.accountData)

      this.signupData.checkExisting(this.longForm.value['basic']['nricno']).subscribe(res => {
        if(res['success'] && res['success'] == 1) {
          this.warningDialog('Existing Customer', 'Our system shows that you are an existing customer. Please contact our customer support')
        } else {
          this.signupData.saveData(this.accountData).subscribe((res) => {
            localStorage.setItem('registration', 'true'); 
            this.nextPage.emit(this.accountData);
          })
        }
      });

      
    } 
  }

    /**
     * After a form is initialized, we link it to our main form
     */
    formInitialized(name: string, form: FormGroup) {
      this.longForm.setControl(name, form);
    }

}
