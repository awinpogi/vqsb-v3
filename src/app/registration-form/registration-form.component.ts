import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DevicesService } from "./../shared/services/devices.service"

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {
  planTypeID = JSON.parse(localStorage.getItem('planTypeID'));
  constructor(
    private router: Router,
    private device: DevicesService
    ) { }

  ngOnInit() {

    this.device.getUserDevice().subscribe((res) => {
      localStorage.setItem("device", res['device'])
      localStorage.setItem("ip", res['ip'])
    })

    localStorage.setItem('registration', 'false')

    if(localStorage.getItem('plandetails') != 'true' && this.router.url.includes('/refer/'))  {
      
      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/refer/freedom/plandetails')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/refer/max/plandetails')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/refer/raptor/plandetails')
      }
      return;

    } else  if(localStorage.getItem('plandetails') != 'true' && this.router.url.includes('/internal/'))  {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/internal/freedom/plandetails')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/internal/max/plandetails')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/internal/raptor/plandetails')
      }
      return;

    } else if (localStorage.getItem('plandetails') != 'true') {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/freedom/plandetails')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/max/plandetails')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/raptor/plandetails')
      }
      return;

    }
  }

  public nextEvent(data: any): void {
    if (this.router.url.includes('/refer/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/refer/freedom/preview');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/refer/max/preview');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/refer/raptor/preview');
      }
    } else if (this.router.url.includes('/internal/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/internal/freedom/preview');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/internal/max/preview');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/internal/raptor/preview');
      }
    } else {
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/freedom/preview');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/max/preview');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/raptor/preview');
      }
    }
  }

}
