import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNricComponent } from './form-nric.component';

describe('FormNricComponent', () => {
  let component: FormNricComponent;
  let fixture: ComponentFixture<FormNricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
