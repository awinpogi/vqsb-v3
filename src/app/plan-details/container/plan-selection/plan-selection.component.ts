import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ChangeDetectionStrategy, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PlansService } from "../../../shared/services/plans.service"; 
import { PlanDetailsService } from "../../../shared/services/plan-details.service";
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-plan-selection',
  templateUrl: './plan-selection.component.html',
  styleUrls: ['./plan-selection.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlanSelectionComponent implements OnInit {


  selectedKey;
  planAppend;
  selectedItem
  initialIndex = localStorage.getItem('init');
  initialPlan = 'mesh';
  initialContent = '';
  formName;
  formEvent;
  isReset = false;
  isOV = false;
  formEventList: Array<{formNameValue: string, formEventValue: FormGroup}> = [];
  formResetList: Array<{resetFrom: boolean}> = [];
  planFormSubmitted = false;
  formKey;
  formRouter;
  formVas
  formBoost
  formRemarks;
  plansData: any;
  planDetailsForm: FormGroup;
  planTypeID = localStorage.getItem('planTypeID');
  signupID = JSON.parse(localStorage.getItem('signupID'));
  planID = JSON.parse(localStorage.getItem('planID'));
  planData:  {}
  upgrade;

  @Input() planIndex:  number;
  @Input() planKey: string;
  @Input() planThumbnail: string;
  @Input() planTitle: string;
  @Input() planSpeed: string;
  @Input() planPrice: string;
  @Input() planPayment: string;
  @Input() planTerm: string;
  @Input() planSpecial: string;
  @Input() planChild: string;
  @Input() planParent;
  @Input() priceDiff;
  @Input() planFreebies: Array<{label: string, price: string}>;
  @Input() planRouters: Array<{key: string, img: string, title: string, price: string, active: boolean}>;
  @Input() planOnevoice: Array<{key: string, label: string}>;
  @Input() planNonDisplay: Array<{key: string, label: string}>;
  @Input() planFibreGuard: Array<{key: string, label: string}>;
  @Input() planInstallation: Array<{key: string, label: string}>;
  @Input() planAddOn: Array<{key: string, label: string}>;
  @Input() planBoost: Array<{key: string, label: string}>;
  @Input() planFdns: Array<{key: string, label: string}>;
  @Input() planImage: Array<{url: string}>;
  @Output() public nextPage = new EventEmitter();
  @Output() public formDataReset = new EventEmitter();
  @ViewChild('upgradedPlanYes') boostClassYes:ElementRef;
  @ViewChild('upgradedPlanNo') boostClassNo:ElementRef;

  
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private planConfig: PlansService,
    private router: Router,
    private savePlanDetails: PlanDetailsService,
    private renderer: Renderer2,
    ) {
   }

  ngOnInit() { 

    this.planFormInitialized(this.planID);
    this.formInitialized(this.formName, this.formEvent);
    this.initialize(this.planTypeID);

  }
  
  config: SwiperOptions = {
    slidesPerView: 'auto',
  };

  // More details modal trigger   
  moreDetails(content,modal) { 

    if(modal == true) {
      this.modalService.open(content, {  centered: true, windowClass: 'planModal'}); 
    } 
  }

  // Change of plan
  changePlan(content,i, plan, modal) {
    // Remove flip class
    this.boostClassYes.nativeElement.classList.remove('cube-flip') 
    this.boostClassNo.nativeElement.classList.remove('cube-flip')

    // localStorage.setItem('init', i); 
    // localStorage.setItem('oldInit', i);
    this.formVas = this.planDetailsForm.controls.vas; 

    if( (this.planTypeID == '1' && parseInt(localStorage.getItem('init')) > 2) || (this.planTypeID == '2' && parseInt(localStorage.getItem('init')) > 0) || (this.planTypeID == '3' && parseInt(localStorage.getItem('init')) > 0) )  {
      this.upgrade = true;
      this.boostClassYes.nativeElement.classList.add('cube-flip') 
      this.boostClassNo.nativeElement.classList.remove('cube-flip')
      this.formVas.controls.oneVoice.setValue(98)
      this.formVas.controls.oneVoice.setErrors(null) 
    } else {
      this.upgrade = false;
      this.boostClassYes.nativeElement.classList.remove('cube-flip') 
      this.boostClassNo.nativeElement.classList.remove('cube-flip')
      this.formVas.controls.oneVoice.setValue('')
      this.formVas.controls.oneVoice.setErrors({
        "requred": true
      })
    }
    this.planAppend = plan[i]; 

    this.planIndex = i;
    this.planParent = this.planAppend['planParent'];
    this.planChild = this.planAppend['planChild'];
    this.planKey = this.planAppend['key'];
    this.planThumbnail = this.planAppend['image'];
    this.planTitle = this.planAppend['name'];
    this.planSpeed = this.planAppend['speed'];
    this.planPrice = this.planAppend['price'];;
    this.planPayment = (this.planAppend['specialShortTerm'] != '' ? this.planAppend['specialShortTerm'] : this.planAppend['shortTerm'])
    this.planTerm = this.planAppend['contract'];
    this.planSpecial = this.planAppend['special'];
    this.planFreebies = this.planAppend['freebies'];
    this.planRouters = this.planAppend['routers'];
    this.planOnevoice = this.planAppend['onevoice'];
    this.planNonDisplay = this.planAppend['nondisplay'];
    this.planFibreGuard = this.planAppend['fibreguard'];
    this.planInstallation = this.planAppend['installation'];
    this.planBoost = this.planAppend['boost'];
    this.planAddOn = this.planAppend['plan_addon'];
    this.planFdns = this.planAppend['fdns'];
    this.planImage = this.planAppend['image']; 
    this.priceDiff = this.planAppend['priceDiff']; 
    localStorage.setItem('planID', this.planKey); 

    console.table(this.planSpeed)
  
    // Trigger modal for plan more details
    if(modal == true) {
      this.modalService.open(content, {  centered: true, windowClass: 'planModal'});
      this.planDetailsForm.reset()
    }

    this.selectedItem=i;

  }


  // Boost plan
  upgradePlan(key, plan, boost) {  

    let i;
    this.formVas = this.planDetailsForm.controls.vas; 
    // // Check if boosted is true or false

    if(boost == true ) {
      this.upgrade = true;
      i = plan.map(function(o) { return o.key; }).indexOf(key); 
      localStorage.setItem('init', i);
      this.formVas.controls.oneVoice.setValue(98)
      this.formVas.controls.oneVoice.setErrors(null) 
      this.boostClassYes.nativeElement.classList.add('cube-flip') 
      this.boostClassNo.nativeElement.classList.remove('cube-flip') 

    } else if(boost == false ) {

      this.upgrade = false;
      i = plan.map(function(o) { return o.key; }).indexOf(key);
      localStorage.setItem('init', i);  
      this.formVas.controls.oneVoice.setValue('')
      this.formVas.controls.oneVoice.setErrors({
        "requred": true
      })

      this.boostClassYes.nativeElement.classList.remove('cube-flip') 
      this.boostClassNo.nativeElement.classList.add('cube-flip') 

    }

    this.planAppend = plan[i]; 

    this.planIndex = i;
    this.planParent = this.planAppend['planParent'];
    this.planChild = this.planAppend['planChild'];
    this.planKey = this.planAppend['key'];
    this.planThumbnail = this.planAppend['image'];
    this.planTitle = this.planAppend['name'];
    this.planSpeed = this.planAppend['speed'];
    this.planPrice = this.planAppend['price'];;
    this.planPayment = this.planAppend['shortTerm'];
    this.planTerm = this.planAppend['contract'];
    this.planSpecial = this.planAppend['special'];
    this.planFreebies = this.planAppend['freebies'];
    this.planRouters = this.planAppend['routers'];
    this.planOnevoice = this.planAppend['onevoice'];
    this.planNonDisplay = this.planAppend['nondisplay'];
    this.planFibreGuard = this.planAppend['fibreguard'];
    this.planInstallation = this.planAppend['installation'];
    this.planBoost = this.planAppend['boost'];
    this.planAddOn = this.planAppend['plan_addon'];
    this.planFdns = this.planAppend['fdns'];
    this.planImage = this.planAppend['image'];
    this.priceDiff = this.planAppend['priceDiff'];
    // this.planDetailsForm.reset()
    this.planDetailsForm.controls.planKey.setValue(this.planKey )
    localStorage.setItem('planID', this.planKey); 

    console.table(this.planSpeed)

    // Add flip effect on button  

    this.selectedItem=i; 

  }

  // Selecting of other plan
  selectPlan(key: string,) {

    this.planFormInitialized(key);
    this.selectedKey = key; 
    this.modalService.dismissAll();

    this.formEventList.forEach(function (value) {
      this.formInitialized( value.formNameValue, value.formEventValue)
    }, this)
    
  }

  /* 
    Initialized the value of plan data
  */
  initialize(id) {
    this.planConfig.getPlanByType(id).subscribe(res => {
      this.plansData = res;  
      this.planConfig.plans = this.planConfig.plans; 
      this.changePlan(this.initialContent,this.initialIndex, this.plansData, false);
    });
  } 

  planFormInitialized(id) {
    // Initialized Main Form
    this.planDetailsForm = this.formBuilder.group({
      planKey: id
    }) 
  }

   /**
   * After a form is initialized, we link it to our main form
   */
  formInitialized(name: string, form: FormGroup) {
    // Initialized Sub form

    this.planDetailsForm.setControl(name, form);
    this.formName = name;
    this.formEvent = form;
    this.formEventList.push({ formNameValue: name, formEventValue: form });  

  }


  /*
    Display error dialog box 
  */
  errorDialog (errorTitle: string, errorText:string) {
    Swal.fire({
      type: 'error',
      title: errorTitle,
      text: errorText
    }) 
  }

  /*
    Display error dialog box  with confirmation button
  */
  errorConfirmDialog() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success mx-2',
        cancelButton: 'btn btn-danger mx-2'
      },
      buttonsStyling: false,
    })

    swalWithBootstrapButtons.fire({
      title: 'Boost your Plan',
      text: "Do you want to boost your plan?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Please',
      cancelButtonText: 'No, Thanks',
      reverseButtons: false
    }).then((result) => {
      if (result.value) {
        this.formBoost.value = true;
        this.formBoost.setErrors(null);
        this.nextButton();
      } else {
        this.formBoost.value = false;
        this.formBoost.setErrors(null);
        this.nextButton();
      }
    })
  }


  /* Submit form */
  nextButton() { 
    this.planFormSubmitted = true;
    this.formKey = this.planDetailsForm.controls.planKey; 
    this.formBoost = this.planDetailsForm.controls.boostPlan; 
    this.formRouter = this.planDetailsForm.controls.router; 
    this.formVas = this.planDetailsForm.controls.vas; 
    this.formRemarks = this.planDetailsForm.controls.remarks;  
    if (this.planDetailsForm.invalid) {

      if ( this.formKey.valid == false ) {

        this.errorDialog('No Plan selected', 'Please select your plan!') 

      } else if ( this.formRouter.valid == false ) {  

        this.errorDialog('No Router selected', 'Please select router!') 

      }  else if ( this.formVas.controls.oneVoice.valid == false ) { 

        this.errorDialog('No OneVoice selected', 'Please select OneVoice!') 

      } else if ( this.formVas.controls.oneVoice.valid == true && this.formVas.controls.ovNonDisplay.valid == false  ) { 

        if (this.formVas.controls.oneVoice.value == 50) {
          this.formVas.controls.ovNonDisplay.setValue(59)
          this.formVas.controls.ovNonDisplay.setErrors(null)
        } else {
          this.formVas.controls.ovNonDisplay.setValue('')
          this.formVas.controls.ovNonDisplay.setErrors({
            'required': true
          })
          this.errorDialog('No Number non-display selected', 'Please select Number non-display!') 
        }
      } else if ( this.formVas.controls.fibreGuard.valid == false ) { 

        this.errorDialog('No Fibre-Guard selected', 'Please select Fibre-Guard!') 

      } else if ( this.formVas.controls.siteInstallation.valid == false ) { 
 
        this.errorDialog('No On-site Installation selected', 'Please select Installation!') 

      } else if ( this.formVas.controls.addOnHardware.valid == false ) { 

        this.errorDialog('No Add-ons selected', 'Please select Add-ons!')  

      } else if ( this.formVas.controls.netflix.valid == false ) { 

        this.errorDialog('No Netflix selected', 'Please select Netflix!')  

      }
      
      } else if(this.planDetailsForm.valid) {

      this.planData = { 
        "plan": this.formKey.value,  
        "router": this.formRouter.value['routerKey'],  
        "oneVoice": this.formVas.controls.oneVoice.value,  
        "nonDisplay": this.formVas.controls.ovNonDisplay.value,  
        "fibreGuard":this.formVas.controls.fibreGuard.value, 
        "onsiteInstallation": this.formVas.controls.siteInstallation.value,  
        "addOn": this.formVas.controls.addOnHardware.value,  
        "fdns":this.formVas.controls.netflix.value, 
        "remarks":this.formRemarks.value['remarks'],  
        "boosted": 1,
        "update_signup_id": this.signupID,
        "ip": localStorage.getItem("ip"),
        "device": localStorage.getItem("device"),
        "signup_flow": 3
      } 
      this.savePlanDetails.saveData(this.planData).subscribe((res) => {
        localStorage.setItem('plandetails', 'true'); 
        this.nextPage.emit(this.planDetailsForm.value); 
      })
    }
  }

}
