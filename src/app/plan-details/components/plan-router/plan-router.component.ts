import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-plan-router',
  templateUrl: './plan-router.component.html',
  styleUrls: ['./plan-router.component.scss']
})
export class PlanRouterComponent implements OnInit {
  
  isClicked = []; 
  selectedItem;
  routerForm: FormGroup; 
  routerKey;
  @Output() formReady = new EventEmitter<FormGroup>();
  @Input() routers;
  @Input() planFormSubmitted;


  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2
  ) { }
  
  ngOnInit() {
    this.routerFormInitialized(this.routerKey);
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   // only run when property "data" changed
  //   if(changes['routers']) {
  //     if(changes.routers.previousValue != undefined) {
  //       this.selectedItem = 100;
  //     }
  //   }
  // }
  

  config: SwiperOptions = {
    slidesPerView: 'auto',
  };

  toggleClass(index, key){
    this.selectedItem=index;
    this.routerKey = key;
    this.routerFormInitialized(this.routerKey);
  }

  routerFormInitialized(key:string) {
    this.routerForm = this.formBuilder.group({
      routerKey: new FormControl(key, Validators.required),
    });

    this.formReady.emit(this.routerForm);
  }
  
}
