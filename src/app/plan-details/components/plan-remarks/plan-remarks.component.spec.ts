import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanRemarksComponent } from './plan-remarks.component';

describe('PlanRemarksComponent', () => {
  let component: PlanRemarksComponent;
  let fixture: ComponentFixture<PlanRemarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanRemarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanRemarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
