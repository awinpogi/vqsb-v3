import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-plan-remarks',
  templateUrl: './plan-remarks.component.html',
  styleUrls: ['./plan-remarks.component.scss']
})
export class PlanRemarksComponent implements OnInit {

  remarksForm: FormGroup;

  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.remarksForm = this.formBuilder.group({
      remarks: ''
    });

    this.formReady.emit(this.remarksForm);
  }
}
