import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanVasComponent } from './plan-vas.component';

describe('PlanVasComponent', () => {
  let component: PlanVasComponent;
  let fixture: ComponentFixture<PlanVasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanVasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanVasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
