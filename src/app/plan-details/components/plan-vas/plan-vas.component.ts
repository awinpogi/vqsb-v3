import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'; 


@Component({
  selector: 'app-plan-vas',
  templateUrl: './plan-vas.component.html',
  styleUrls: ['./plan-vas.component.scss']
  
})
export class PlanVasComponent implements OnInit {

  vasForm: FormGroup;
  @Output() formReady = new EventEmitter<FormGroup>();
  @Input() onevoice: Array<{key: string, label: string}>;
  @Input() nondisplay: Array<{key: string, label: string}>;
  @Input() fibreguard: Array<{key: string, label: string}>;
  @Input() installation: Array<{key: string, label: string}>;
  @Input() addon: Array<{key: string, label: string}>;
  @Input() boost: Array<{key: string, label: string}>;
  @Input() fdns: Array<{key: string, label: string}>;
  @Input() resetFormData: boolean;
  @Input() planFormSubmitted;
  @Input() upgrade;
  defaultOV
  ov 

  constructor(
    private formBuilder: FormBuilder
  ) {  }

  ngOnInit() { 
    this.vasFormInit()
  }


  get f() { return this.vasForm.controls; }

  vasFormInit() {
    this.vasForm = this.formBuilder.group({
      oneVoice: new FormControl('', Validators.required),
      ovNonDisplay: new FormControl('', Validators.required),
      fibreGuard: new FormControl('', Validators.required),
      siteInstallation: new FormControl('', Validators.required),
      addOnHardware: new FormControl('', Validators.required),
      netflix: new FormControl('Singapore', Validators.required),
    });
    this.formReady.emit(this.vasForm);
  }


}
