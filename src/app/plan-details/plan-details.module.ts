import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms"; 
import { SlickCarouselModule } from "ngx-slick-carousel";
import { SwiperModule } from "angular2-useful-swiper"; 
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PlansService } from "./../shared/services/plans.service";
import { PlanDetailsService } from "./../shared/services/plan-details.service";
import { DevicesService } from "./../shared/services/devices.service";

import { PlanDetailsComponent } from "./plan-details.component";
import { PlanSelectionComponent } from './container/plan-selection/plan-selection.component';
import { PlanRouterComponent } from './components/plan-router/plan-router.component';
import { PlanVasComponent } from './components/plan-vas/plan-vas.component';
import { PlanRemarksComponent } from './components/plan-remarks/plan-remarks.component';
 

@NgModule({
  declarations: [
    PlanDetailsComponent,
    PlanSelectionComponent,
    PlanRouterComponent,
    PlanVasComponent,
    PlanRemarksComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, 
    SlickCarouselModule,
    SwiperModule,
    NgbModule
  ],
  exports: [
    PlanDetailsComponent
  ],
  providers: [
    PlansService,
    PlanDetailsService,
    DevicesService
  ],
})
export class PlanDetailsModule { }
