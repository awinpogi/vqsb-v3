import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"; 
import { DevicesService } from "./../shared/services/devices.service"

@Component({
  selector: 'app-plan-details',
  templateUrl: './plan-details.component.html',
  styleUrls: ['./plan-details.component.scss']
})
export class PlanDetailsComponent implements OnInit {

  planTypeID = JSON.parse(localStorage.getItem('planTypeID'));
  constructor(
    private router: Router,
    private device: DevicesService
    ) { }

  ngOnInit() {

    this.device.getUserDevice().subscribe((res) => {
      localStorage.setItem("device", res['device'])
      localStorage.setItem("ip", res['ip'])
    })

    localStorage.setItem('plandetails', 'false')
    

    if(localStorage.getItem('shortform') != 'true' && this.router.url.includes('/refer/'))  { 

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/refer/freedom/signup')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/refer/max/signup')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/refer/raptor/signup')
      }
      return;

    } else  if(localStorage.getItem('shortform') != 'true' && this.router.url.includes('/internal/'))  { 
      
      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/internal/freedom/signup')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/internal/max/signup')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/internal/raptor/signup')
      }
      return;

    } else if (localStorage.getItem('shortform') != 'true') {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/freedom/signup')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/max/signup')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/raptor/signup')
      }
      return;
      
    }
  }

  nextEvent() {
    if (this.router.url.includes('/refer/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/refer/freedom/registration');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/refer/max/registration');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/refer/raptor/registration');
      }
    } else if (this.router.url.includes('/internal/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/internal/freedom/registration');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/internal/max/registration');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/internal/raptor/registration');
      }
    } else {
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/freedom/registration');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/max/registration');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/raptor/registration');
      }
    }
  }
}
