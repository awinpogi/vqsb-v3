import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from "@angular/router"; 
import { DevicesService } from "./../shared/services/devices.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  constructor(
    private router: Router,
    private device: DevicesService
    ) { }

  ngOnInit() {

    //localStorage.clear();
    //localStorage.setItem('home', 'false');
    this.device.getUserDevice().subscribe((res) => {
      localStorage.setItem("device", res['device'])
      localStorage.setItem("ip", res['ip'])
    })
  } 
}
