import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { SignupService } from './../shared/services/signup.service';
import { DevicesService } from "./../shared/services/devices.service"; 

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from "./home.component";
import { PlanRaptorComponent } from './components/plan-raptor/plan-raptor.component';
import { PlanOrbiMeshComponent } from './components/plan-orbi-mesh/plan-orbi-mesh.component';
import { PlanOrbiSmartMeshComponent } from './components/plan-orbi-smart-mesh/plan-orbi-smart-mesh.component';
import { PlanAsusMeshComponent } from './components/plan-asus-mesh/plan-asus-mesh.component';
import { AccordionComponent } from './container/accordion/accordion.component'; 
import { PlanMaxOnegbpsComponent } from './components/plan-max-onegbps/plan-max-onegbps.component';
import { VqsbPlanMbps24Component } from './components/vqsb-plan-mbps24/vqsb-plan-mbps24.component';
import { VqsbPlanMbps12Component } from './components/vqsb-plan-mbps12/vqsb-plan-mbps12.component';
import { VqsbPlanGbps12Component } from './components/vqsb-plan-gbps12/vqsb-plan-gbps12.component';
import { VqsbPlanGbps24Component } from './components/vqsb-plan-gbps24/vqsb-plan-gbps24.component';


@NgModule({
  declarations: [
    HomeComponent,
    PlanRaptorComponent,
    PlanOrbiMeshComponent,
    PlanOrbiSmartMeshComponent,
    PlanAsusMeshComponent,
    AccordionComponent, 
    PlanMaxOnegbpsComponent, VqsbPlanMbps24Component, VqsbPlanMbps12Component, VqsbPlanGbps12Component, VqsbPlanGbps24Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, 
    SharedModule, 
    NgbModule
  ],
  providers: [
    SignupService,
    DevicesService
  ],
  exports: [
    HomeComponent
  ]
})

export class HomeModule { }
