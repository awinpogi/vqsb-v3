import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VqsbPlanMbps24Component } from './vqsb-plan-mbps24.component';

describe('VqsbPlanMbps24Component', () => {
  let component: VqsbPlanMbps24Component;
  let fixture: ComponentFixture<VqsbPlanMbps24Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VqsbPlanMbps24Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VqsbPlanMbps24Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
