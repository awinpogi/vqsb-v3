import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Signup } from "./../../../shared/models/signup.model";
import { SignupService } from "./../../../shared/services/signup.service";
import { DevicesService } from "./../../../shared/services/devices.service";
import { Observable, Subject } from 'rxjs';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-vqsb-plan-mbps24',
  templateUrl: './vqsb-plan-mbps24.component.html',
  styleUrls: ['./vqsb-plan-mbps24.component.css']
})
export class VqsbPlanMbps24Component implements OnInit {

    @Input() planValue: any;
    posts$: Observable<Signup[]>;
    @Output() public plan = new EventEmitter();
    signupId;

    constructor(
        private router: Router,
        private signup: SignupService,
        private device: DevicesService,
        private modalService: NgbModal,
    ) {
    }

    ngOnInit() { }


    selectedPlan() {
        
        this.signupId = {
            "plan_id": 0,
            "ip": localStorage.getItem("ip"),
            "device": localStorage.getItem("device"),
            //"building": localStorage.getItem("myBuilding"),
            //"agent": localStorage.getItem("myAgent"),
        }

        this.signup.createSignup(this.signupId).subscribe((res) => {
            localStorage.setItem('signupID', JSON.stringify(res['signup_id']))
            localStorage.setItem('planID', JSON.stringify(res['plan_id']))
            localStorage.setItem('init', '0')
            localStorage.setItem('home', 'true');
            //console.log(localStorage);
            this.router.navigateByUrl('/freedom/signup');
        })
    }

    /*
      Display modal
    */
    checkPlan(content, modal) {

        if (modal == true) {
            this.modalService.open(content, { centered: true, windowClass: 'planModal' });
        }

    }

    /*
      Close modal
    */
    closePlan() {
        this.modalService.dismissAll()
    }

}
