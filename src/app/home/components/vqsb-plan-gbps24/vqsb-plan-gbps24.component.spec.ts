import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VqsbPlanGbps24Component } from './vqsb-plan-gbps24.component';

describe('VqsbPlanGbps24Component', () => {
  let component: VqsbPlanGbps24Component;
  let fixture: ComponentFixture<VqsbPlanGbps24Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VqsbPlanGbps24Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VqsbPlanGbps24Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
