import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VqsbPlanMbps12Component } from './vqsb-plan-mbps12.component';

describe('VqsbPlanMbps12Component', () => {
  let component: VqsbPlanMbps12Component;
  let fixture: ComponentFixture<VqsbPlanMbps12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VqsbPlanMbps12Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VqsbPlanMbps12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
