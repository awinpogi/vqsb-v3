import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Signup } from "./../../../shared/models/signup.model";
import { SignupService } from "./../../../shared/services/signup.service";
import { DevicesService } from "./../../../shared/services/devices.service";
import { Observable, Subject } from 'rxjs'; 
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-plan-max-onegbps',
  templateUrl: './plan-max-onegbps.component.html',
  styleUrls: ['./plan-max-onegbps.component.css']
})
export class PlanMaxOnegbpsComponent implements OnInit {

  @Input() planValue: any;
  posts$: Observable<Signup[]>;
  @Output() public plan = new EventEmitter(); 
  signupId

  constructor(
    private router: Router,
    private signup: SignupService,
    private device: DevicesService,
    private modalService: NgbModal
  ) { 
  }

  ngOnInit() { }
 
  selectedPlan () {  

    this.signupId = {
      "plan_id": 2,
      "ip": localStorage.getItem("ip"),
      "device": localStorage.getItem("device"),
    } 

    this.signup.createSignup(this.signupId).subscribe((res) => {
      localStorage.setItem('signupID', JSON.stringify(res['signup_id'])) 
      localStorage.setItem('planID', JSON.stringify(res['plan_id']))  
      localStorage.setItem('init', '0') 
      localStorage.setItem('home', 'true');
      
      if (this.router.url.includes('/refer/'))  {  
        this.router.navigateByUrl('/refer/max/signup');
      } else if (this.router.url.includes('/internal/'))  { 
        this.router.navigateByUrl('/internal/max/signup');
      }  else {
        this.router.navigateByUrl('/max/signup');
      }
    })
  }

  /*
    Display modal
  */
 checkPlan(content,modal) { 

  if(modal == true) {
  this.modalService.open(content, {  centered: true, windowClass: 'planModal'}); 
  } 

}

/*
  Close modal
*/
closePlan() {  
  this.modalService.dismissAll()
}



  

}
