import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanMaxOnegbpsComponent } from './plan-max-onegbps.component';

describe('PlanMaxOnegbpsComponent', () => {
  let component: PlanMaxOnegbpsComponent;
  let fixture: ComponentFixture<PlanMaxOnegbpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanMaxOnegbpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanMaxOnegbpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
