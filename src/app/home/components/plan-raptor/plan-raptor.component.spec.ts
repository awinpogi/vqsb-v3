import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanRaptorComponent } from './plan-raptor.component';

describe('PlanRaptorComponent', () => {
  let component: PlanRaptorComponent;
  let fixture: ComponentFixture<PlanRaptorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanRaptorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanRaptorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
