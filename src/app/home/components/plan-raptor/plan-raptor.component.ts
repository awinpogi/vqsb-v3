import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Signup } from "./../../../shared/models/signup.model";
import { SignupService } from "./../../../shared/services/signup.service";
import { DevicesService } from "./../../../shared/services/devices.service";
import { Observable, Subject } from 'rxjs'; 
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-plan-raptor',
  templateUrl: './plan-raptor.component.html',
  styleUrls: ['./plan-raptor.component.scss']
})
export class PlanRaptorComponent implements OnInit {

  @Input() planValue: any;
  posts$: Observable<Signup[]>;
  @Output() public plan = new EventEmitter(); 
  signupId

  constructor(
    private router: Router,
    private signup: SignupService,
    private device: DevicesService,
    private modalService: NgbModal
  ) { 
  }

  ngOnInit() { } 

  selectedPlan () {  
    
    this.signupId = {
      "plan_id": 7,
      "ip": localStorage.getItem("ip"),
      "device": localStorage.getItem("device"),
    }  

    this.signup.createSignup(this.signupId).subscribe((res) => {
      localStorage.setItem('signupID', JSON.stringify(res['signup_id'])) 
      localStorage.setItem('planID', JSON.stringify(res['plan_id']))  
      localStorage.setItem('init', '0')
      localStorage.setItem('home', 'true');
      
      if (this.router.url.includes('/refer/'))  {  
        this.router.navigateByUrl('/refer/raptor/signup');
      } else if (this.router.url.includes('/internal/'))  {  
        this.router.navigateByUrl('/internal/raptor/signup');
      } else {
        this.router.navigateByUrl('/raptor/signup');
      }
    })
  }

  /*
    Display modal
  */
 checkPlan(content,modal) { 

  if(modal == true) {
  this.modalService.open(content, {  centered: true, windowClass: 'planModal'}); 
  } 

}

/*
  Close modal
*/
closePlan() {  
  this.modalService.dismissAll()
}



  

}
