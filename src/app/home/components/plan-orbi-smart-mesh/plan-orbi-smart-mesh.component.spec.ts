import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOrbiSmartMeshComponent } from './plan-orbi-smart-mesh.component';

describe('PlanOrbiSmartMeshComponent', () => {
  let component: PlanOrbiSmartMeshComponent;
  let fixture: ComponentFixture<PlanOrbiSmartMeshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanOrbiSmartMeshComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOrbiSmartMeshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
