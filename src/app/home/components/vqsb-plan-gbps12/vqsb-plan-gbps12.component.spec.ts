import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VqsbPlanGbps12Component } from './vqsb-plan-gbps12.component';

describe('VqsbPlanGbps12Component', () => {
  let component: VqsbPlanGbps12Component;
  let fixture: ComponentFixture<VqsbPlanGbps12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VqsbPlanGbps12Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VqsbPlanGbps12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
