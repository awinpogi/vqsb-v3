import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOrbiMeshComponent } from './plan-orbi-mesh.component';

describe('PlanOrbiMeshComponent', () => {
  let component: PlanOrbiMeshComponent;
  let fixture: ComponentFixture<PlanOrbiMeshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanOrbiMeshComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOrbiMeshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
