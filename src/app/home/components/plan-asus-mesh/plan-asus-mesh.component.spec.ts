import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanAsusMeshComponent } from './plan-asus-mesh.component';

describe('PlanAsusMeshComponent', () => {
  let component: PlanAsusMeshComponent;
  let fixture: ComponentFixture<PlanAsusMeshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanAsusMeshComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanAsusMeshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
