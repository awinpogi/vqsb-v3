import { Component, OnInit, Renderer2, ElementRef, Input } from '@angular/core';
import { PlansService } from "../../../shared/services/plans.service"; 
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.css']
})
export class AccordionComponent implements OnInit {
  public isCollapsedone = false;
  plansData: any;

  @Input() isFreedom = false;
  @Input() isMax = false;
  @Input() isGamer = false;
  planType: number; 
  id: string;
  utmSource;
  utmCampaign;
  utmMedium;
  utmTerms
  utmContent
  
  constructor(
    private planConfig: PlansService,
    private router: Router,
    private utmRoutes: ActivatedRoute
  ) {
  }

  ngOnInit() {
    
    this.checkRoutes(); 
    this.plansData = [];
    this.initialize();
    this.getUtms()
    
  }

  /* 
    Initialized the value of plan data
  */
    initialize() {

    this.planConfig.getPlanByType(this.planType).subscribe(res => {
      this.plansData = res;
        this.planConfig.plans = this.planConfig.plans; 
        //console.log(res);
    });
  } 

  // Get utm values 
  getUtms() {

    this.utmSource = (this.utmRoutes.snapshot.queryParamMap.get('utm_source') != null ) ? this.utmRoutes.snapshot.queryParamMap.get('utm_source') : '-';
    localStorage.setItem('utmSource', this.utmSource);

    this.utmCampaign = (this.utmRoutes.snapshot.queryParamMap.get('utm_campaign') != null ) ? this.utmRoutes.snapshot.queryParamMap.get('utm_campaign') : '-';
    localStorage.setItem('utmCampaign', this.utmCampaign);

    this.utmMedium = (this.utmRoutes.snapshot.queryParamMap.get('utm_medium') != null ) ? this.utmRoutes.snapshot.queryParamMap.get('utm_medium') : '-';
    localStorage.setItem('utmMedium', this.utmMedium);

    this.utmTerms = (this.utmRoutes.snapshot.queryParamMap.get('utm_terms') != null ) ? this.utmRoutes.snapshot.queryParamMap.get('utm_terms') : '-';
    localStorage.setItem('utmTerms', this.utmTerms);

    this.utmContent = (this.utmRoutes.snapshot.queryParamMap.get('utm_content') != null ) ? this.utmRoutes.snapshot.queryParamMap.get('utm_content') : '-';
    localStorage.setItem('utmContent', this.utmContent); 

  }
  

  // Check the url to identify the plan type id
  checkRoutes() {
    if (this.router.url.includes('/freedom'))  {  

      this.isFreedom = true; 
      this.planType = 1; 
      this.id = this.router.url.split('/').pop(); 
      localStorage.setItem('planTypeID','1')

    } else if (this.router.url.includes('/max')) {
        
      this.isMax = true; 
      this.planType = 2; 
      this.id = this.router.url.split('/').pop();  
      localStorage.setItem('planTypeID','2') 
      
    } else if (this.router.url.includes('/raptor'))  { 

      this.isGamer = true; 
      this.planType = 3; 
      this.id = this.router.url.split('/').pop();
      localStorage.setItem('planTypeID','3') 
    } 
  }
}
