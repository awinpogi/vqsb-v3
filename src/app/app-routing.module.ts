import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { PlanDetailsComponent } from './plan-details/plan-details.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { PreviewComponent } from './preview/preview.component';
import { PaymentComponent } from './payment/payment.component';
import { SuccessPageComponent } from './success-page/success-page.component';
import { LocationComponent } from './location/location.component';
import { NameEditorComponent } from './name-editor/name-editor.component';
import { CybPaymentComponent } from './cyb-payment/cyb-payment.component';
import { CybtestComponent } from './cybtest/cybtest.component';
import { CybsuccessComponent } from './cybsuccess/cybsuccess.component';

@NgModule({
  imports: [],
  exports: [RouterModule]
})

export class AppRoutingModule { }

/*
  Main OSP Routes
*/
export const MainRoutes: Routes = [
  
  // Homepage
  { 
    path: '',
    component: HomeComponent,
    children: [                          //<---- child components declared here
      {
          path:'freedom',
          component: HomeComponent
      },
      {
        path:'max',
        component: HomeComponent
      },
      {
        path:'raptor', 
        component: HomeComponent
      } 
    ]
  },

  // Short Form 
  { path: 'freedom/signup', component: SignupFormComponent },
  { path: 'max/signup', component: SignupFormComponent },
  { path: 'raptor/signup', component: SignupFormComponent },

  // Plan Details  
  { path: 'freedom/plandetails', component: PlanDetailsComponent },
  { path: 'max/plandetails', component: PlanDetailsComponent },
  { path: 'raptor/plandetails', component: PlanDetailsComponent },

  // Registration 
  { path: 'freedom/registration', component: RegistrationFormComponent },
  { path: 'max/registration', component: RegistrationFormComponent },
  { path: 'raptor/registration', component: RegistrationFormComponent },

  // Preview
  { path: 'freedom/preview', component: PreviewComponent },
  { path: 'max/preview', component: PreviewComponent },
  { path: 'raptor/preview', component: PreviewComponent },

  // Payment
  { path: 'freedom/payment', component: PaymentComponent },
  { path: 'max/payment', component: PaymentComponent },
  { path: 'raptor/payment', component: PaymentComponent },

  // Success 
  { path: 'freedom/success-old', component: SuccessPageComponent },
  { path: 'max/success-old', component: SuccessPageComponent },
  { path: 'raptor/success-old', component: SuccessPageComponent },
  
  // Location 
  { path: 'location', component: LocationComponent },

  // Reactive Test 
  { path: 'formtest', component: NameEditorComponent },

  // CYB Payment
  { path: 'cybpayment', component: CybPaymentComponent },

  // CYB Payment Test
    { path: 'cybtest', component: CybtestComponent },

     // CYB Payment Success
    { path: 'success', component: CybsuccessComponent }
];

/* 
  Refer OSP Routes
*/
export const ReferRoutes: Routes = [

  // Homepage
  { 
    path: '',
    component: HomeComponent,
    children: [                          //<---- child components declared here
      {
          path:'refer/freedom',
          component: HomeComponent
      },
      {
        path:'refer/max',
        component: HomeComponent
      },
      {
        path:'refer/raptor',
        component: HomeComponent
      } 
    ]
  },

  // Short Form 
  { path: 'refer/freedom/signup', component: SignupFormComponent },
  { path: 'refer/max/signup', component: SignupFormComponent },
  { path: 'refer/raptor/signup', component: SignupFormComponent },

  // Plan Details  
  { path: 'refer/freedom/plandetails', component: PlanDetailsComponent }, 
  { path: 'refer/max/plandetails', component: PlanDetailsComponent },
  { path: 'refer/raptor/plandetails', component: PlanDetailsComponent },

  // Registration  
  { path: 'refer/freedom/registration', component: RegistrationFormComponent },
  { path: 'refer/max/registration', component: RegistrationFormComponent },
  { path: 'refer/raptor/registration', component: RegistrationFormComponent },

  // Preview 
  { path: 'refer/freedom/preview', component: PreviewComponent }, 
  { path: 'refer/max/preview', component: PreviewComponent },
  { path: 'refer/raptor/preview', component: PreviewComponent },

  // Payment 
  { path: 'refer/freedom/payment', component: PaymentComponent },
  { path: 'refer/raptor/payment', component: PaymentComponent },
  { path: 'refer/max/payment', component: PaymentComponent },

  // Succes 
  { path: 'refer/freedom/success', component: SuccessPageComponent },
  { path: 'refer/max/success', component: SuccessPageComponent },
  { path: 'refer/raptor/success', component: SuccessPageComponent }

];

/* 
  Internal OSP Routes
*/
export const InternalRoutes: Routes = [

  // Homepage
  { 
    path: '',
    component: HomeComponent,
    children: [                          //<---- child components declared here
      {
          path:'internal/freedom',
          component: HomeComponent
      },
      {
        path:'internal/max',
        component: HomeComponent
      },
      {
        path:'internal/raptor',
        component: HomeComponent
      } 
    ]
  },

  // Short Form 
  { path: 'internal/freedom/signup', component: SignupFormComponent },
  { path: 'internal/max/signup', component: SignupFormComponent },
  { path: 'internal/raptor/signup', component: SignupFormComponent},

  // Plan Details  
  { path: 'internal/freedom/plandetails', component: PlanDetailsComponent },
  { path: 'internal/max/plandetails', component: PlanDetailsComponent },
  { path: 'internal/raptor/plandetails', component: PlanDetailsComponent },

  // Registration  
  { path: 'internal/freedom/registration', component: RegistrationFormComponent },
  { path: 'internal/max/registration', component: RegistrationFormComponent },
  { path: 'internal/raptor/registration', component: RegistrationFormComponent },

  // Preview 
  { path: 'internal/freedom/preview', component: PreviewComponent },
  { path: 'internal/max/preview', component: PreviewComponent },
  { path: 'internal/raptor/preview', component: PreviewComponent },

  // Payment 
  { path: 'internal/freedom/payment', component: PaymentComponent },
  { path: 'internal/max/payment', component: PaymentComponent },
  { path: 'internal/raptor/payment', component: PaymentComponent },

  // Success
  { path: 'internal/freedom/success', component: SuccessPageComponent },
  { path: 'internal/max/success', component: SuccessPageComponent },
  { path: 'internal/raptor/success', component: SuccessPageComponent }

];