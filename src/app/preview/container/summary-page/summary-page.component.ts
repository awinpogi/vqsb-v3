import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PreviewService } from "../../../shared/services/preview.service";
import { UploadService } from "../../../shared/services/upload.service";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.scss']
})
export class SummaryPageComponent implements OnInit {
  previewForm: FormGroup 
  safData = {};
  customerDetails
  planDetails
  tncForm = false;
  signupID = JSON.parse(localStorage.getItem('signupID'));
  @Output() public nextPage = new EventEmitter();


  constructor(
    private formBuilder: FormBuilder,
    private previewData: PreviewService,
    private generateSAF: UploadService,
    private saveSAF: PreviewService
    ) { }

  ngOnInit() {
    this.previewData.getSAFData(this.signupID).subscribe(res => {  
    }); 
    this.initialize(this.signupID)
    this.previewForm = this.formBuilder.group({
      tnc:['',
      [ 
          Validators.required
        ]
      ],
    }) 
  }

   /* 
    Initialized the customer data
  */
 initialize(id) {
   
  // Get data to display in preview
  this.previewData.showPreview(id).subscribe(res => {
      this.customerDetails = res['customerDetails'];
      console.log(res);
    this.planDetails = res['orderDetails'];    
  });  
} 


  /*
  Display error dialog box 
  */
  errorDialog (errorTitle: string, errorText:string) {
    Swal.fire({
      type: 'error',
      title: errorTitle,
      text: errorText
    }) 
  }

  /* 
    Diplay confirmation dialog
  */
 confirmDialog() {
    Swal.fire({
      title: 'Proceed to Payment',
      text: "You will be redirected to the payment page, it might take a few seconds. Please do not refresh the page or click the ‘Back’ or Close button of your browser.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Proceed'
    }).then((result) => {
      if (result.value) { 
        const formData = new FormData(); 
        formData.append('saf_data', 'Test only');
        this.generateSAF.generateSAF(formData).subscribe(res => { 
          // Get data to save in saf
            this.previewData.getSAFData(this.signupID).subscribe(res => {  
              if(res != null) {
                formData.append('saf_data', JSON.stringify(res));
                this.generateSAF.generateSAF(formData).subscribe(SAFres => {
                  this.safData = {
                    "update_signup_id": this.signupID,
                    "saf_url": SAFres['safURL'],
                    "ip": localStorage.getItem("ip"),
                    "device": localStorage.getItem("device"),
                    "signup_flow": 5,
                  }; 
                  this.saveSAF.saveSAFURL(this.safData).subscribe(saveSAF => { 
                    localStorage.setItem('preview', 'true'); 
                      this.nextPage.emit(this.previewForm.value);
                      console.log(this.safData);
                  }); 
                  // this.nextPage.emit(this.previewForm.value);
                }); 
              }
            });  
        }); 
      }
    })
  }

  // Proceed to next page
  public nextButton() {
    this.tncForm = true;
    if( this.previewForm.invalid ) {
      this.errorDialog('Terms & Conditions', 'You must accept the Term & Accept before you can proceed')
      return false;
    } else  {
      this.confirmDialog()
    }

  }

}