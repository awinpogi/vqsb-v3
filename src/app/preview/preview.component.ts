import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DevicesService } from "./../shared/services/devices.service";

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  planTypeID = JSON.parse(localStorage.getItem('planTypeID'))

  constructor(
    private router: Router,
    private device: DevicesService
    ) { }

  ngOnInit() {

    this.device.getUserDevice().subscribe((res) => {
      localStorage.setItem("device", res['device'])
      localStorage.setItem("ip", res['ip'])
    })

    localStorage.setItem('preview', 'false')

    /*if(localStorage.getItem('registration') != 'true' && this.router.url.includes('/refer/'))  {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/refer/freedom/registration')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/refer/max/registration')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/refer/raptor/registration')
      }
      return;

    } else  if(localStorage.getItem('registration') != 'true' && this.router.url.includes('/internal/'))  {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/internal/freedom/registration')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/internal/max/registration')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/internal/raptor/registration')
      }
      return;
      
    } else if (localStorage.getItem('registration') != 'true') {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/freedom/registration')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/max/registration')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/raptor/registration')
      }
      return;

    }*/
  }

  public nextEvent(data: any): void {
   /*if (this.router.url.includes('/refer/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/refer/freedom/payment');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/refer/max/payment');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/refer/raptor/payment');
      }
    } else if (this.router.url.includes('/internal/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/internal/freedom/payment');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/internal/max/payment');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/internal/raptor/payment');
      }
    } else {
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/freedom/payment');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/max/payment');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/raptor/payment');
      }
    }*/
      this.router.navigateByUrl('/cybpayment');
  }

}
