import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms"; 
import { PreviewService } from "../shared/services/preview.service";
import { UploadService } from "../shared/services/upload.service";
import { DevicesService } from "./../shared/services/devices.service";

import { PreviewComponent } from "./preview.component";
import { SummaryPageComponent } from './container/summary-page/summary-page.component';
import { UserSummaryComponent } from './components/user-summary/user-summary.component';
import { TotalSummaryComponent } from './components/total-summary/total-summary.component';

@NgModule({
  declarations: [
    PreviewComponent,
    SummaryPageComponent,
    UserSummaryComponent,
    TotalSummaryComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    PreviewComponent
  ],
  providers: [
    PreviewService,
    UploadService,
    DevicesService
  ]
})
export class PreviewModule { }