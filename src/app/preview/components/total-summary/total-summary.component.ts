import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-total-summary',
  templateUrl: './total-summary.component.html',
  styleUrls: ['./total-summary.component.scss']
})
export class TotalSummaryComponent implements OnInit {

  @Input() previewInfo;
  constructor() { }

  ngOnInit() {
  }

}
