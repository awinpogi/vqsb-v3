import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ViewEncapsulation, ChangeDetectionStrategy, Renderer2 } from '@angular/core';
import { CybpaymentService } from '../shared/services/cybpayment.service';
import { Payment } from './../cyb-payment/payment';
import { PaymentService } from "../shared/services/payment.service"; 
import { DevicesService } from "../shared/services/devices.service";
import { Router } from '@angular/router';


@Component({
  selector: 'app-cybtest',
  templateUrl: './cybtest.component.html',
  styleUrls: ['./cybtest.component.css']
})
export class CybtestComponent implements OnInit {


    constructor(private cybPayment: CybpaymentService, private paymentConfig: PaymentService, private device: DevicesService, private router: Router) { }

    reference_number: string;
    transaction_type: string;
    amount: string;
    currency: string;
    payment_method: string;
    access_key: string;
    profile_id: string;
    signed_date_time: string;
    signed_field_names: string;
    transaction_uuid: string;
    bill_to_forename: string;
    bill_to_surname: string;
    bill_to_email: string;
    bill_to_phone: string;
    bill_to_address_line1: string;
    bill_to_address_city: string;
    bill_to_address_state: string;
    bill_to_address_country: string;
    bill_to_address_postal_code: string;
    signature: string;
    locale: string;
    signeddate: string;
    unsigned_field_names: string;
    submitted = false;
    ccPaymentData = {};
    paymentResult = {};
    signupID = JSON.parse(localStorage.getItem('signupID'));
    @ViewChild('payment_confirmation') d1: ElementRef;

    ngOnInit() {

        this.cybPayment.performGetEX().subscribe(res => {
            
            var el = '<input type="hidden" name="transaction_type" value="' + res['transaction_type'] + '"></input><br><input type="hidden" name="reference_number" value="' + res['reference_number'] + '"></input><br><input type="hidden" name="amount" value="' + res['amount'] + '"></input><br><input type="hidden" name="currency" value="' + res['currency'] + '"></input><br><input type="hidden" name="payment_method" value="' + res['payment_method'] + '"></input><br><input type="hidden" name="bill_to_forename" value="' + res['bill_to_forename'] + '"></input><br><input type="hidden" name="bill_to_surname" value="' + res['bill_to_surname'] + '"></input><br><input type="hidden" name="bill_to_email" value="' + res['bill_to_email'] + '"></input><br><input type="hidden" name="bill_to_phone" value="' + res['bill_to_phone'] + '"></input><br><input type="hidden" name="bill_to_address_line1" value="' + res['bill_to_address_line1'] + '"></input><br><input type="hidden" name="bill_to_address_city" value="' + res['bill_to_address_city'] + '"></input><br><input type="hidden" name="bill_to_address_state" value="' + res['bill_to_address_state'] + '"></input><br><input type="hidden" name="bill_to_address_country" value="' + res['bill_to_address_country'] + '"></input><br><input type="hidden" name="bill_to_address_postal_code" value="' + res['bill_to_address_postal_code'] + '"></input><br><input type="hidden" name="access_key" value="' + res['access_key'] + '"></input><br><input type="hidden" name="signed_date_time" value="' + res['signed_date_time'] + '"></input><br><input type="hidden" name="signed_field_names" value="' + res['signed_field_names'] + '"></input><br><input type="hidden" name="unsigned_field_names" value="' + res['unsigned_field_names'] + '"></input><br><input type="hidden" name="locale" value="' + res['locale'] + '"></input><br><input type="hidden" name="profile_id" value="' + res['profile_id'] + '"></input><br><input type="hidden" name="transaction_uuid" value="' + res['transaction_uuid'] + '"></input><br><input type="hidden" name="submit" value="' + res['submit'] + '"></input><br><input type="hidden" name="signature" value="' + res['signature'] + '"></input><br><input type="hidden" name="submit" value="Submit"></input>';
            //$('form').append(el);
            //this.renderer.appendChild('form', el);
            //console.log(el);
            this.d1.nativeElement.insertAdjacentHTML('beforeend', el);
            /*
            this.transaction_type = res['transaction_type'];
            this.amount = res['amount'];
            this.access_key = res['access_key'];
            this.signed_date_time = res['signed_date_time'];
            this.signed_field_names = res['signed_field_names'];
            this.unsigned_field_names = res['unsigned_field_names'];
            this.currency = res['currency'];
            this.locale = res['locale'];
            this.payment_method = res['payment_method'];
            this.bill_to_forename = res['bill_to_forename'];
            this.bill_to_surname = res['bill_to_surname'];
            this.bill_to_email = res['transaction_type'];
            this.bill_to_phone = res['bill_to_phone'];
            this.bill_to_address_line1 = res['bill_to_address_line1'];
            this.bill_to_address_city = res['bill_to_address_city'];
            this.bill_to_address_state = res['bill_to_address_state'];
            this.bill_to_address_country = res['bill_to_address_country'];
            this.bill_to_address_postal_code = res['bill_to_address_postal_code'];
            this.signature = res['signature'];
            this.signeddate = res['signed_date_time'];
            this.transaction_uuid = res['transaction_uuid'];
            this.profile_id = res['profile_id'];
            this.reference_number = res['reference_number'];*/
            /*cybPostData.access_key = res['access_key'];
            cybPostData.amount = res['amount'];
            cybPostData.bill_to_address_city = res['bill_to_address_city'];
            cybPostData.reference_number = res['reference_number'];
            cybPostData.bill_to_address_country = res['bill_to_address_country'];
            cybPostData.bill_to_address_line1 = res['bill_to_address_line1'];
            cybPostData.bill_to_address_postal_code = res['bill_to_address_postal_code'];
            cybPostData.bill_to_address_state = res['bill_to_address_state'];
            cybPostData.bill_to_email = res['bill_to_email'];
            cybPostData.bill_to_forename = res['bill_to_forename'];
            cybPostData.bill_to_phone = res['bill_to_phone'];
            cybPostData.bill_to_surname = res['bill_to_surname'];
            cybPostData.currency = res['currency'];
            cybPostData.locale = res['locale'];
            cybPostData.payment_method = res['payment_method'];
            cybPostData.profile_id = res['profile_id'];
            cybPostData.signature = res['signature'];
            cybPostData.signed_date_time = res['signed_date_time'];
            cybPostData.signed_field_names = res['signed_field_names'];
            cybPostData.transaction_type = res['transaction_type'];
            cybPostData.transaction_uuid = res['transaction_uuid'];*/
            console.log(res);
        });
        
      
  }

}
