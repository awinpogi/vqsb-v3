import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CybtestComponent } from './cybtest.component';

describe('CybtestComponent', () => {
  let component: CybtestComponent;
  let fixture: ComponentFixture<CybtestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CybtestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CybtestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
