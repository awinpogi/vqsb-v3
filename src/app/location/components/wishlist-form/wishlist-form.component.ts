import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-wishlist-form',
  templateUrl: './wishlist-form.component.html',
  styleUrls: ['./wishlist-form.component.css']
})
export class WishlistFormComponent implements OnInit {

    @ViewChild('wishListBuilding') wishListBuilding: ElementRef;
    @ViewChild('wishListCity') wishListCity: ElementRef;
    @ViewChild('wishListState') wishListState: ElementRef;
    @ViewChild('wishListName') wishListName: ElementRef;
    @ViewChild('wishListEmail') wishListEmail: ElementRef;


    wishListData: {}

  constructor() { }

  ngOnInit() {
  }
  
  displayWishList() {
      this.wishListData = {
          "wishListBuilding": this.wishListBuilding.nativeElement.value,
          "wishListCity": this.wishListCity.nativeElement.value,
          "wishListState": this.wishListState.nativeElement.value,
          "wishListName": this.wishListName.nativeElement.value,
          "wishListEmail": this.wishListEmail.nativeElement.value,
      }
      console.log(this.wishListData);
  }

  hideWishList() {
   alert('hide wishlist');
  }

}
