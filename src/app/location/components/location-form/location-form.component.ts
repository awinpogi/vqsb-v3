import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.css']
})

export class LocationFormComponent implements OnInit {
  constructor() { }

  public myLocation:Array<Object> = [
		{id:1, myarea:'Taman Sri Raya', mystreet:'No.1, Persiaran Sri Raya, Batu 9 Cheras',mybuilding:'Saville @ Cheras',mycity:'Cheras',mypostcode:'43200',mystate:'Selangor'},
		{id:2, myarea:'', mystreet:'',mybuilding:'Setia Eco Glades',mycity:'Cyberjaya',mypostcode:'63000',mystate:'Selangor'},
		{id:3, myarea:'N/A', mystreet:'Jalan Dato Abdullah Tahir',mybuilding:'Setia Sky 88',mycity:'Johor Bahru',mypostcode:'80300',mystate:'Johor'},
		{id:4, myarea:'N/A', mystreet:'Jalan Trus Off Jalan Wong Ah Fook',mybuilding:'Suasana Iskandar',mycity:'Johor Bahru',mypostcode:'80000',mystate:'Johor'},
		{id:5, myarea:'Persiaran Tropicana', mystreet:'Tropicana Gold & Country Resort Pju 3',mybuilding:'Tropicana Avenue',mycity:'Petaling Jaya',mypostcode:'47810',mystate:'Selangor'},
		{id:6, myarea:'Kota Damansara', mystreet:'Jalan Pju 3/21',mybuilding:'Tropicana Gardens',mycity:'Petaling Jaya',mypostcode:'47810',mystate:'Selangor'},
		{id:7, myarea:'Taman Abad', mystreet:'Jalan Dato Abdullah Tahir',mybuilding:'Twin Galaxy',mycity:'Johor Bahru',mypostcode:'80300',mystate:'Johor'},
	];
  
  ngOnInit() {
  }
  	
  nextButton() { 
	alert('next');
  }
}
