import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"; 
import { DevicesService } from "./../shared/services/devices.service"

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

 constructor (
    private router: Router, 
    private device: DevicesService
  ) { }

  ngOnInit() {
  }
  

}
