import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LocationComponent } from './location.component';
import { LocationFormComponent } from './components/location-form/location-form.component';
import { LocationModComponent } from './container/location-mod/location-mod.component';
import { WishlistFormComponent } from './components/wishlist-form/wishlist-form.component';
//import { PlanRouterComponent } from './../plan-details/components/plan-router/plan-router.component';


@NgModule({
  declarations: [
  LocationComponent,
  LocationFormComponent,
  LocationModComponent,
  //PlanRouterComponent,
  WishlistFormComponent],
  imports: [
    CommonModule,
	FormsModule,
	ReactiveFormsModule
  ]
})
export class LocationModule { }
