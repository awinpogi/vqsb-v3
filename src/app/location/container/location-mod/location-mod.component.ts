import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { WishlistFormComponent } from "../../components/wishlist-form/wishlist-form.component";
import { LocationFormComponent } from "../../components/location-form/location-form.component";
import { Router } from "@angular/router"; 
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import Swal from 'sweetalert2' 

@Component({
  selector: 'app-location-mod',
  templateUrl: './location-mod.component.html',
  styleUrls: ['./location-mod.component.css']
})

export class LocationModComponent implements OnInit {	
	
	@ViewChild(WishlistFormComponent) wishList:WishlistFormComponent; 
    @ViewChild(LocationFormComponent) locationList: LocationFormComponent;
    @ViewChild('areaInput') areaInput: ElementRef;
    @ViewChild('streetInput') streetInput: ElementRef;
    @ViewChild('buildingInput') buildingInput: ElementRef;
    @ViewChild('cityInput') cityInput: ElementRef;
    @ViewChild('postcodeInput') postcodeInput: ElementRef;
    @ViewChild('stateInput') stateInput: ElementRef;
	
	@Input() isWishListForm = false;
	@Input() isLocationForm = false;

	@Output() formReady = new EventEmitter<FormGroup>();
	
	unitNumber: string;
	myArea: string;
	myStreet: string;
	myBuilding: string;
	myCity: string;
	myPostCode: string;
	myState: string;
	locationForm: FormGroup;
	locationFormSubmitted = false;
    name: string = "";
	
	validation_message = {
    'unitnumber_error': [
       { type: 'maxlength', message: 'Unit number cannot be more than 2 characters long' },
      { type: 'pattern', message: 'FlooUnitr number must contain only numbers' }
    ]
  }
  
  
  constructor (
    private router: Router,
	private formBuilder: FormBuilder,
	
  ) { }
  

  ngOnInit() {
  }
  
  dataForm(){
	this.locationForm = this.formBuilder.group({
		unitNumber: ['', Validators.required],
		formArea: ['', Validators.required]
	});
   // this.formReady.emit(this.locationForm); 
   console.log(this.locationForm.controls)
  }
   get f() { return this.locationForm.controls; }

  private myarea:string;
  private mystreet:string;
  private mybuilding:string;
  private mycity: string;
  private mypostcode: string;
  private mystate: string;
  private myAgent: string;
  private isReadOnly:boolean;


  
  selectLocation(event: any){
	if(event.target.value == "Please Select"){
		this.isLocationForm = false;
		this.myarea = null;
		this.mystate = '';
		this.mystreet = '';
		this.mypostcode = null;
		this.mycity = '';
		this.mybuilding = '';
	}
	else{
		this.isLocationForm = true;
		this.isWishListForm = false;
		if(event.target.value == "Saville @ Cheras"){
			this.isReadOnly = true;
			this.myarea = "Taman Sri Raya";
			this.mystreet = "No.1, Persiaran Sri Raya, Batu 9 Cheras";
			this.mybuilding = "Saville @ Cheras";
			this.mycity = "Cheras";
			this.mypostcode = "43200";
			this.mystate = "Selangor";
		}
		else if(event.target.value == "Setia Eco Glades"){
			this.myarea = "";
			this.mystreet = "";
			this.mybuilding = "Setia Eco Glades";
			this.mycity = "Cyberjaya";
			this.mypostcode = "63000";
			this.mystate = "Selangor";
		}
		else if(event.target.value == "Setia Sky 88"){
			this.myarea = "N/A";
			this.mystreet = "Jalan Dato Abdullah Tahir";
			this.mybuilding = "Setia Sky 88";
			this.mycity = "Johor Bahru";
			this.mypostcode = "80300";
			this.mystate = "Johor";
		}
		else if(event.target.value == "Suasana Iskandar"){
			this.myarea = "N/A";
			this.mystreet = "Jalan Trus Off Jalan Wong Ah Fook";
			this.mybuilding = "Suasana Iskandar";
			this.mycity = "Johor Bahru";
			this.mypostcode = "80000";
			this.mystate = "Johor";
		}
		else if(event.target.value == "Tropicana Avenue"){
			this.myarea = "Persiaran Tropicana";
			this.mystreet = "Tropicana Gold & Country Resort Pju 3";
			this.mybuilding = "Tropicana Avenue";
			this.mycity = "Petaling Jaya";
			this.mypostcode = "47810";
			this.mystate = "Selangor";
		}
		else if(event.target.value == "Tropicana Gardens"){
			this.myarea = "Kota Damansara";
			this.mystreet = "Jalan Pju 3/21";
			this.mybuilding = "Tropicana Gardens";
			this.mycity = "Petaling Jaya";
			this.mypostcode = "47810";
			this.mystate = "Selangor";
		}
		else if(event.target.value == "Twin Galaxy"){
			this.myarea = "Taman Abad";
			this.mystreet = "Jalan Dato Abdullah Tahir";
			this.mybuilding = "Twin Galaxy";
			this.mycity = "Johor Bahru";
			this.mypostcode = "80300";
			this.mystate = "Johor";
		}
	
    }
      if (event.target.value == "Setia Sky 88" || event.target.value == "Suasana Iskandar" || event.target.value == "Twin Galaxy") {
          this.myAgent = "Damian Tay";
      }
      else if (event.target.value == "Saville @ Cheras") {
          this.myAgent = "Daniel Abraham";
      }
      else if (event.target.value == "Setia Eco Glades" || event.target.value == "Tropicana Avenue" || event.target.value == "Tropicana Gardens") {
          this.myAgent = "Mark Valerio";
      }
      else {
          this.myAgent = "Direcct";
      }
	this.dataForm();
  }
  
  displayWishList(){
	  this.isWishListForm = true;
	  this.isLocationForm = false;
  }
  
  /*
    Display error dialog box 
  */
  errorDialog (error: string, errorTitle: string, errorText:string) {
    if(error == "success") {

      Swal.fire({
        type: 'success',
        title: errorTitle,
        text: errorText
      }) 

    } else {
      Swal.fire({
        type: 'error',
        title: errorTitle,
        text: errorText
      }) 
    }
  }
    nextButton() { 
        this.locationFormSubmitted = true;

        if (this.locationForm.controls['unitNumber']['value'].replace(/\s/g, "") == '') {
            this.errorDialog('error', 'Unit Number is Empty ', 'Please input your Unit Number to proceed.');
        }
        else {
            localStorage.setItem('myUnit', this.locationForm.controls['unitNumber']['value']);
            localStorage.setItem('myArea', this.areaInput.nativeElement.value);
            localStorage.setItem('myStreet', this.streetInput.nativeElement.value);
            localStorage.setItem('myCity', this.cityInput.nativeElement.value);
            localStorage.setItem('myBuilding', this.buildingInput.nativeElement.value);
            localStorage.setItem('myPostCode', this.postcodeInput.nativeElement.value);
            localStorage.setItem('myState', this.stateInput.nativeElement.value);
            localStorage.setItem('myAgent', this.myAgent);
            localStorage.setItem('myAddress', this.locationForm.controls['unitNumber']['value'] + ' ' + this.areaInput.nativeElement.value + ' ' + this.streetInput.nativeElement.value + ' ' + this.cityInput.nativeElement.value + ' ' + this.buildingInput.nativeElement.value);
            //save the location field variables and redirect customer to the plan selection page
            this.router.navigateByUrl('/freedom');
        }
	
  }

  

  
  
}
