import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationModComponent } from './location-mod.component';
import { Store, StoreModule } from '@ngrx/store';

describe('LocationModComponent', () => {
  let component: LocationModComponent;
  let fixture: ComponentFixture<LocationModComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ LocationModComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationModComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
