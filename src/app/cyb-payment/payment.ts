export class Payment {
    public access_key: string;
    amount: string;
    currency: string;
    locale: string;
    payment_method: string;
    profile_id: string;
    reference_number: string;
    signature: string;
    signed_date_time: string;
    signed_field_names: string;
    transaction_type: string;
    transaction_uuid: string;
    card_type: string;
    public card_number: string;
    card_cvn: string;
    card_expiry_date: string;
    unsigned_field_names: string;
    bill_to_address_city: string;
    bill_to_address_country: string;
    bill_to_address_line1: string;
    bill_to_address_postal_code: string
    bill_to_address_state: string;
    bill_to_email: string;
    bill_to_forename: string;
    bill_to_phone: string;
    bill_to_surname: string;
}
