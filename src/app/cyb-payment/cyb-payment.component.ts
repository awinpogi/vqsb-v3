import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { CybpaymentService } from '../shared/services/cybpayment.service'; 
import { Payment } from './payment';
import { PaymentService } from "../shared/services/payment.service"; 
import { DevicesService } from "../shared/services/devices.service"; 
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';


@Component({
  selector: 'app-cyb-payment',
  templateUrl: './cyb-payment.component.html',
  styleUrls: ['./cyb-payment.component.css']
})
export class CybPaymentComponent implements OnInit {

    objPayment: any;
    cardFullName: string;
    paymentData: any;
    namePattern = '^[a-zA-Z0-9 ]+$';
    phonePattern = '^[0-9]+$';
    creditCardPattern = '^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$';
    cardCVVPattern = '/^[0-9]{3,4}$/';
    submitted = false;
    ccPaymentData = {};

    signupID = JSON.parse(localStorage.getItem('signupID'));

    @Input() isVisa = false;
    @Input() isMastercard = false;
    @Input() isAmericanExpress = false;


    paymentForm = this.fb.group({
        cardNumber: ['', [
            Validators.required,
            Validators.minLength(16),
            Validators.maxLength(16),
            Validators.pattern(this.creditCardPattern),
        ]],
        cardType: [''],
        cardFullName: ['', Validators.required],
        cardCVV: ['', Validators.required],
        cardCountry: ['', Validators.required],
        cardExpMonth: ['', Validators.required],
        cardExpYear: ['', Validators.required],
        cardAddress: ['', Validators.required],
        cardState: ['', Validators.required],
        cardCity: ['', Validators.required],
        cardPhoneNum: ['', [
            Validators.required,
            Validators.minLength(9),
            Validators.maxLength(11),
            Validators.pattern(this.phonePattern),
        ]],
        cardEmailAdd: ['', [ 
            Validators.email,
            Validators.required
          ]]
    });


    constructor(private fb: FormBuilder, private cybPayment: CybpaymentService, private paymentConfig: PaymentService, private device: DevicesService, private router: Router) { }
    get f() { return this.paymentForm.controls; }

    ngOnInit() {
        
    }
    onSubmit() {
        this.paymentForm.value['cardType'] = this.GetCardType(this.paymentForm.value['cardNumber']);
        var cybPostData = new Payment();
        this.cybPayment.performGetEX().subscribe(res => {
            cybPostData.access_key = res['access_key'];
            cybPostData.amount = res['amount'];
            cybPostData.bill_to_address_city = res['bill_to_address_city'];
            cybPostData.reference_number = res['reference_number'];
            cybPostData.bill_to_address_country = res['bill_to_address_country'];
            cybPostData.bill_to_address_line1 = res['bill_to_address_line1'];
            cybPostData.bill_to_address_postal_code = res['bill_to_address_postal_code'];
            cybPostData.bill_to_address_state = res['bill_to_address_state'];
            cybPostData.bill_to_email = res['bill_to_email'];
            cybPostData.bill_to_forename = res['bill_to_forename'];
            cybPostData.bill_to_phone = res['bill_to_phone'];
            cybPostData.bill_to_surname = res['bill_to_surname'];
            cybPostData.currency = res['currency'];
            cybPostData.locale = res['locale'];
            cybPostData.payment_method = res['payment_method'];
            cybPostData.profile_id = res['profile_id'];
            cybPostData.signature = res['signature'];
            cybPostData.signed_date_time = res['signed_date_time'];
            cybPostData.signed_field_names = res['signed_field_names'];
            cybPostData.transaction_type = res['transaction_type'];
            cybPostData.transaction_uuid = res['transaction_uuid'];
            cybPostData.card_type = this.paymentForm.value['cardType'];
            cybPostData.card_number = this.paymentForm.value['cardNumber'];
            cybPostData.card_cvn = this.paymentForm.value['cardCVV']
            cybPostData.card_expiry_date = this.paymentForm.value['cardExpMonth'] + "-" + this.paymentForm.value['cardExpYear']
        });
        console.log(cybPostData);
        this.submitted = true;

        /*this.cybPayment.postCybData(cybPostData).subscribe(
            data => {
                this.objPayment = data;
            }
        )*/
        if (!this.paymentForm.valid) {
            //console.log('form is invalid');
        } 
        else {
            this.ccPaymentData = {
                "accept_t_n_c": 1,
                "cc_payment": "4028e488361ddbfc01362c21e8fe3aba",
                "creditCardAddress1": "Tulo, Batangas City",
                "creditCardCountry": "Malaysia",
                "creditCardCity": "Kuala Lumpur",
                "creditCardPostalCode": "59200",
                "email": "sherwinricalde@gmail.com",
                "phone": "01136801393",
                "paymentResponse": 'Success',
                "autoPay": "false",
                "update_signup_id": this.signupID,
                "ip": localStorage.getItem("ip"),
                "device": localStorage.getItem("device"),
                "signup_flow": 5,  
                "Entity__c": "VQSB",
                "currency": "MYR",
                "notes": "MY Account",
                "batch": "Batch16",
                "invoiceTemplateId": "2c92a0fd58048c850158195b22a54e93",
                "communicationProfileId": "2c92a0ff6300cc93016315d051092730",
                "paymentGateway": "myvqsb-SOAP"
            }
            console.log(this.ccPaymentData);
            this.paymentConfig.savePayment(this.ccPaymentData).subscribe((res) => {
                if (res) {

                    this.device.getUserDevice().subscribe((res) => {
                        localStorage.setItem("device", res['device']);
                        localStorage.setItem("ip", res['ip']);
                    })

                    this.router.navigateByUrl('/success');
                }

            })
        }
    }
   

   GetCardType(number) {
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null)
        return "001";

    // Mastercard 
    // Updated for Mastercard 2017 BINs expansion
    if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
        return "002";
        
    // AMEX
    re = new RegExp("^3[47]");
    if (number.match(re) != null)
        return "003";

    // Discover
    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
        return "Discover";

    // Diners
    re = new RegExp("^36");
    if (number.match(re) != null)
        return "Diners";

    // Diners - Carte Blanche
    re = new RegExp("^30[0-5]");
    if (number.match(re) != null)
        return "Diners - Carte Blanche";

    // JCB
    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
        return "JCB";

    // Visa Electron
    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
    if (number.match(re) != null)
        return "Visa Electron";

    return "";
   }



}
