import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CybPaymentComponent } from './cyb-payment.component';

describe('CybPaymentComponent', () => {
  let component: CybPaymentComponent;
  let fixture: ComponentFixture<CybPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CybPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CybPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
