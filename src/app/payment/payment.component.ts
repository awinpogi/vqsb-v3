import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DevicesService } from "./../shared/services/devices.service"

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  planTypeID = JSON.parse(localStorage.getItem('planTypeID'))
  constructor(
    private router: Router,
    private device: DevicesService
    ) { }

  ngOnInit() {

    this.device.getUserDevice().subscribe((res) => {
      localStorage.setItem("device", res['device'])
      localStorage.setItem("ip", res['ip'])
    })


    localStorage.setItem('payment', 'false')

    if(localStorage.getItem('preview') != 'true' && this.router.url.includes('/refer/'))  {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/refer/freedom/preview')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/refer/max/preview')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/refer/raptor/preview')
      }
      return;

    } else  if(localStorage.getItem('preview') != 'true' && this.router.url.includes('/internal/'))  {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/internal/freedom/preview')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/internal/max/preview')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/internal/raptor/preview')
      }
      return;
      
    } else if (localStorage.getItem('preview') != 'true') {

      if(this.planTypeID == "1") {
        this.router.navigateByUrl('/freedom/preview')
      } else if(this.planTypeID == "2") {
        this.router.navigateByUrl('/max/preview')
      } else if(this.planTypeID == "3") {
        this.router.navigateByUrl('/raptor/preview')
      }
      return;

    }
  }

  public nextEvent(data: any): void {
    if (this.router.url.includes('/refer/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/refer/freedom/success');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/refer/max/success');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/refer/raptor/success');
      }
    } else if (this.router.url.includes('/internal/'))  {  
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/internal/freedom/success');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/internal/max/success');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/internal/raptor/success');
      }
    } else {
      if(this.planTypeID == 1) {
        this.router.navigateByUrl('/freedom/success');
      } else if(this.planTypeID == 2) {
        this.router.navigateByUrl('/max/success');
      } else if(this.planTypeID == 3) {
        this.router.navigateByUrl('/raptor/success');
      }
    }
  }

}
