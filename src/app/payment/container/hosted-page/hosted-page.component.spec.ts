import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostedPageComponent } from './hosted-page.component';

describe('HostedPageComponent', () => {
  let component: HostedPageComponent;
  let fixture: ComponentFixture<HostedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
