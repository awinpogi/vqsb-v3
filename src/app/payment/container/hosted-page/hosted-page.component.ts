import { Component, OnInit, Input, Output, EventEmitter,  Directive, HostListener  } from '@angular/core'; 
import { FormGroup, FormBuilder } from '@angular/forms'; 

@Component({
  selector: 'app-hosted-page',
  templateUrl: './hosted-page.component.html',
  styleUrls: ['./hosted-page.component.scss']
})
export class HostedPageComponent implements OnInit {

  @Output() public nextPage = new EventEmitter();
  constructor( ) { }

  ngOnInit() {
    
  }

  public nextButton(): void {
    this.nextPage.emit('clicked');
  }

}
