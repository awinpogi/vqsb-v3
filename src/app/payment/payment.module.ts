import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms"; 
import { PaymentService } from "./../shared/services/payment.service";  
import { DevicesService } from "./../shared/services/devices.service";

import { PaymentComponent } from "./payment.component";
import { UserCreditCardComponent } from './components/user-credit-card/user-credit-card.component';
import { HostedPageComponent } from './container/hosted-page/hosted-page.component'; 

 
@NgModule({
  declarations: [
    PaymentComponent,
    UserCreditCardComponent,
    HostedPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule 
  ],
  providers: [
    PaymentService,
    DevicesService
  ],
  exports: [
    PaymentComponent
  ]
})
export class PaymentModule { }
