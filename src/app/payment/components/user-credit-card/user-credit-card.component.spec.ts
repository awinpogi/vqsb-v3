import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCreditCardComponent } from './user-credit-card.component';

describe('UserCreditCardComponent', () => {
  let component: UserCreditCardComponent;
  let fixture: ComponentFixture<UserCreditCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCreditCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCreditCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
