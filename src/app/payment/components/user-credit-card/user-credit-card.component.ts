import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy, AfterViewChecked, SimpleChanges } from '@angular/core';
import { PaymentService } from "../../../shared/services/payment.service"; 
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Payment } from 'src/app/shared/models/payment.model';
import { Router } from '@angular/router';
declare function renderPage(params, prepopulateFields, callback): any; 

@Component({
  selector: 'app-user-credit-card',
  templateUrl: './user-credit-card.component.html',
  styleUrls: ['./user-credit-card.component.scss']
})
export class UserCreditCardComponent implements OnInit {

    @Output() formReady = new EventEmitter<FormGroup>();

    nationalityData: any[];
    paymentForm: FormGroup;
    submitted = false;
    @Input() isLongForm = true;
    @Input() paymentFormSubmitted;
    @Input() longFormSubmitted;
    @Input() info;
    creditCardNumber: string;
    fullName: string;
    emailAddress: string;
    phoneNumber: number;
    city: string;
    country: string;


    namePattern = '^[a-zA-Z0-9 ]+$';
    phonePattern = '^[0-9]+$';

    validation_message = {
        'cardNum': [
            { type: 'required', message: 'Card number is required.' },
            { type: 'minlength', message: 'Please input your card number.' },
            { type: 'maxlength', message: 'Card number cannot be more than 32 numbers. ' },
            { type: 'pattern', message: 'Card number must contain only numbers' }
        ],
        'fullName': [
            { type: 'required', message: 'Name is required.' },
            { type: 'minlength', message: 'Full name must be at least 5 characters long' },
            { type: 'maxlength', message: 'Full name cannot be more than 40 characters long' },
            { type: 'pattern', message: 'Full name must contain only numbers and letters' }
        ],
        'email': [
            { type: 'required', message: 'Email is required' },
            { type: 'email', message: 'Email must be valid' }
        ],
        'mobile': [
            { type: 'required', message: 'Mobile number is required' },
            { type: 'minlength', message: 'Mobile number must be at least 8 characters long' },
            { type: 'maxlength', message: 'Mobile number cannot be more than 8 characters long' },
            { type: 'pattern', message: 'Mobile must contain only numbers' }
        ],
        'dob': [
            { type: 'required', message: 'Birthdate is required' },
            { type: 'date', message: 'Birthday must be valid date' }
        ],
        'gender': [
            { type: 'required', message: 'Gender is required' },
        ],
        'nric': [
            { type: 'required', message: 'NRIC # is required' },
            { type: 'minlength', message: 'NRIC # must be valid' },
            { type: 'pattern', message: 'NRIC # must contain only numbers and letters' }
        ],
        'nationality': [
            { type: 'required', message: 'Nationality is required' },
        ],
        'postalcode': [
            { type: 'required', message: 'Postal code is required' },
            { type: 'minlength', message: 'Postal code must be at least 6 number long' },
            { type: 'maxlength', message: 'Postal code cannot be more than 6 number long' },
            { type: 'pattern', message: 'Postal code must contain only numbers' }
        ],
        'floorno': [
            { type: 'maxlength', message: 'Floor number cannot be more than 3 characters long' },
            { type: 'pattern', message: 'Floor number must contain only numbers and letters' }
        ],
        'unitno': [
            { type: 'maxlength', message: 'Unit number cannot be more than 2 characters long' },
            { type: 'pattern', message: 'FlooUnitr number must contain only numbers' }
        ],
        'nricfront': [
            { type: 'required', message: 'NRIC Front copy is required' },
        ],
        'nricback': [
            { type: 'required', message: 'NRIC Back copy is required' },
        ],
        'promoter': [
            { type: 'required', message: 'Promoter Name is required' },
        ],
        'others': [
            { type: 'required', message: 'Promoter Name is required' },
            { type: 'pattern', message: 'Promoter Name must contain only numbers and letters' }
        ]
    }
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.dataForm();

    }

    get f() { return this.paymentForm.controls; }

    ngOnChanges(changes: SimpleChanges) {
        alert('test');
        if (this.isLongForm == true && changes['info']) {
            if (changes['info'].currentValue != "") {
                // console.log(changes['info'].currentValue)
                this.creditCardNumber = changes['info'].currentValue['CardNum'];
                //this.lastName = changes['info'].currentValue['LastName'];
                //this.emailAddress = changes['info'].currentValue['PersonEmail'];
                //this.phoneNumber = changes['info'].currentValue['Phone'];

                this.dataForm();
            }
        }
    }

    /*
      Initialized Form input 
    */

    dataForm() {
        this.paymentForm = this.formBuilder.group({
            cardNum: [this.creditCardNumber, Validators.required],
            });
     

        this.formReady.emit(this.paymentForm);
    }
    // Proceed to next page
    nextButton() {
        console.log('process payment');
    }
}
  
