import { Component, OnInit } from '@angular/core';
import { PaymentService } from "../shared/services/payment.service"; 
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-cybsuccess',
  templateUrl: './cybsuccess.component.html',
  styleUrls: ['./cybsuccess.component.css']
})
export class CybsuccessComponent implements OnInit {

    paymentResult = {};
    signupID = JSON.parse(localStorage.getItem('signupID'));

    constructor(private paymentConfig: PaymentService, private http: Http) { }

    ngOnInit() {
        this.paymentResult = {
            "update_signup_id": this.signupID,
            "ip": localStorage.getItem("ip"),
            "device": localStorage.getItem("device"),
            "signup_flow": 6
        };
        this.paymentConfig.saveSucess(this.paymentResult).subscribe((result) => {
            localStorage.clear();
           
        })
    }
  



}
