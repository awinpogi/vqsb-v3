import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CybsuccessComponent } from './cybsuccess.component';

describe('CybsuccessComponent', () => {
  let component: CybsuccessComponent;
  let fixture: ComponentFixture<CybsuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CybsuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CybsuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
